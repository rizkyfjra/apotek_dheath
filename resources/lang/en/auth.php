<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Maaf.. Data yang anda masukkan salah.',
    'throttle' => 'Terlalu banyak percobaan login, harap coba lagi dalam beberapa saat.',

];
