@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Master Data</li>
            <li class="breadcrumb-item active">Signa</li>
        </ol>

        <!-- Page Content -->
        <!-- Icon Cards-->
        <a class="btn btn-success btn-sm" href="javascript:void(0)" id="create"><i class="fas fa-wd fa-plus"></i> Tambah
            Data Signa</a>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-hover w-100">
                <thead class="thead-light">
                    <tr>
                        <th scope="col" width="5%">No</th>
                        <th scope="col" width="12%">Signa Kode</th>
                        <th scope="col" width="8%">Nama Signa</th>
                        <th scope="col" width="9%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
                {{-- Modal Form Input --}}
<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalHeader"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form" name="form" class="form-horizontal">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="signa_kode" class="control-label">Kode Signa</label>
                            <input type="text" class="form-control" id="signa_kode" name="signa_kode"
                                placeholder="Kode Signa" autocomplete="off">
                            <span id="signa_ode-error" class="help-block text-danger p-1"></span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="signa_nama" class="control-label">Nama Signa</label>
                            <input type="text" class="form-control" id="signa_nama" name="signa_nama"
                                placeholder="Tempat Lahir Pasien" autocomplete="off">
                            <span id="signa_nama-error" class="help-block text-danger p-1"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function() {
        // Set Up header, token CSRF Cross-Site Request Forgery
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Set Data Table
        var table = $('.table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('signa.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'signa_kode', name:'signa_kode'},
                {data: 'signa_nama', name:'signa_nama'},
                {data: 'action', name:'action', orderable: false, searchable: false}
            ]
        });
        // Trigger form create
        $('#create').click(function(){
            $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
            $('#signa_id').val('');
            $('#form').trigger('reset');
            $('#modalHeader').text('Penambahan Signa');
            $('#modal').modal('show');
            $('.help-block').empty();
        });
        // Trigger form edit
        $('body').on('click', '.editData', function () {
            var id = $(this).data('id');
            $.get("{{ route('jenis-obat.index') }}"+'/'+id+'/edit', function(data) {
                $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                $('#id').val(data.id);
                $('#modalHeader').text('Sunting Data Jenis Obat '+data.jenis_obat);
                $('#modal').modal('show');
                $('.help-block').empty();
                $('#jenis_obat').val(data.jenis_obat);
            })
        });
        // Proses Create dan Update
        $('#saveBtn').click(function (e) {
            e.preventDefault();
            var signa_nama = $('#signa_nama').val();
            var signa_kode = $('#signa_kode').val();
            $(this).html('<i class="fa fa-wd fa-clock-o"></i> Memproses..');
            $(this).attr('disabled',false);
            
            $.ajax({
                data: $('#form').serialize(),
                url: "{{ route('signa.store') }}",
                type: "POST",
                beforeSend: function () {
                    swal({
                        title: 'Menunggu',
                        html: 'Memproses data',
                        onOpen: () => {
                            swal.showLoading()
                        }
                    })
                },
                dataType: 'json',
                success: function (data) {
                    swal({
                        title: "Berhasil!",
                        text: data.success,
                        type: "success"
                    });
                    $('#form').trigger('reset');
                    $(this).html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                    $(this).attr('disabled', false);
                    $('#modal').modal('hide');
                    table.draw();
                }
            });
            
        });

        // hapus data
        $('body').on('click', '.deleteData', function () {
            var signa_id = $(this).data('signa_id');
            swal({
                title: "Apa Anda yakin untuk Menghapus Data ini?",
                text: "Data tidak dapat Dikembalikan setelah Terhapus",
                type: "error",
                showCancelButton: true,
                cancelButtonClass: '#DD6B55',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Hapus',
            }).then(function (e) {
                if(e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'DELETE',
                        url: "{{ url('signa') }}"+'/'+signa_id,
                        data: {_token: CSRF_TOKEN},
                        beforeSend :function () {
                            swal({
                                title: 'Menunggu',
                                html: 'Memproses data',
                                onOpen: () => {
                                swal.showLoading()
                                }
                            })
                        },
                        dataType: 'JSON',
                        success: function(data) {
                            swal(
                                'Berhasil',
                                'Data Berhasil Dihapus!',
                                'success'
                            );
                            table.draw();
                        }
                    })
                }
                else
                {
                    swal(
                        'Batal',
                        'Anda membatalkan penghapusan',
                        'error'
                    );
                }
            })
        })
    });
</script>
@endsection
