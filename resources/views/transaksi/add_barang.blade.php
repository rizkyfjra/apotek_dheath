
@extends('master')
@section('konten')

<div class="container">
    <div class="row">
        @if ($message = Session::get('gagal'))
        <br>
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<strong>{{ $message }}</strong>
				</div>
                @endif
    </div>
    <div class="row">


        <form action="/transaksi/beli_barang" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col">
                    <label>ID Barang</label>
                    <input type="text" name="id_barang" class="form-control" value="{{ $barang->id_barang }}" readonly>
                    @if($errors->has('id_barang'))
                    <div class="text-danger">
                        {{ $errors->first('id_barang')}}
                    </div>
                @endif

                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label>Nama Barang</label>
                <input type="text" name="nama_barang" class="form-control" value="{{ $barang->nama_barang }}" readonly>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label>Harga Barang</label>
                    <input type="text" name="harga_barang" class="form-control" value="{{ $barang->harga_barang }}"  readonly>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label>Jumlah Barang</label>
                    <input type="number" name="jumlah_barang" class="form-control" >
                    @if($errors->has('jumlah_barang'))
                    <div class="text-danger">
                        {{ $errors->first('jumlah_barang')}}
                    </div>
                @endif

                </div>
            </div>
            <div class="row">
                <div class="col">

                    <input type="submit" class="btn btn-success" value="Tambahkan">
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
