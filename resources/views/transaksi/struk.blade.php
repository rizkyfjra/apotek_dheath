<html>
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/bootstrap/css/bootstrap-reboot.min.css" rel="stylesheet" type="text/css">
    <link href="/bootstrap/css/bootstrap-grid.min.css" rel="stylesheet" type="text/css">


    <head>
        <title>Struk</title>
    </head>
    <body>
        <h2>Unnamed</h2>
        <br>
            <h4>No Transaksi : {{ $transaksi->id_transaksi }}</h4>


        <style type="text/css">
        @media print {
        .btn {
    visibility: hidden;
  }

  }
}
        </style>

        <br>
        <table class="table">
            <thead>
                <th>ID Barang</th>
                <th>Nama Barang</th>
                <th>Harga Barang</th>
                <th>Jumlah</th>
                <th>Total Harga</th>
            </thead>
            <tbody>
                @foreach($dbarang as $row)
                <tr>
                    <td>{{ $row['id_barang'] }}</td>
                    <td>{{ $row['nama_barang'] }}</td>
                    <td>{{ $row['harga_barang'] }}</td>
                    <td>{{ $row['jumlah'] }}</td>
                    <td>{{ $row['total_harga'] }}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="4">Total Bayar</td>
                    <td> {{ number_format($total_bayar) }} </td>
                </tr>
                <tr>
                    <td colspan="4">Jumlah Bayar</td>
                    <td> {{ number_format($jumlah_bayar) }} </td>
                </tr>
                <tr>
                    <td colspan="4">Kembalian</td>
                    <td> {{ number_format($kembalian) }} </td>
                </tr>
            </tbody>

        </table>
        <button class="btn btn-success " id="print"> Print </button>
        <a href="/transaksi/add" class="btn btn-primary " >Lakukan Transaksi Lagi</a>
        <a href="/" class="btn btn-primary" >Kembali Ke Dasbor</a>
        <a href="/transaksi" class="btn btn-primary" >List Transaksi</a>

    </body>
    <script src="/jquery.js" type="text/javascript"></script>
    <script src="/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript">
    $("#print").click(function(){
        window.print();
    });
    </script>

</html>
