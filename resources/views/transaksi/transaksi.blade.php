<html>
@extends('master')
@section('konten')
    <head><title>Transaksi</title></head>
    <body>
        <div class="modal fade" id="transaksi_add" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Tambah Barang </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <table class="table table-bordered">
                            <thead>
                                <th>ID Barang</th>
                                <th>Nama Barang</th>
                                <th>Harga Barang</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach($barang as $row)

                                <tr>
                                <td>{{$row->id_barang}}</td>
                                <td>{{$row->nama_barang}}</td>
                                <td>{{$row->harga_barang}}</td>
                                <td><a class="btn btn-success" href="/transaksi/add_barang/{{$row->id_barang}}">Tambahkan</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                        </div>
                </div>

              </div>
            </div>
          </div>
        <div class="container">
            <div class="row">
                <div class="card">
                    <div class="card-header">Transaksi</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="">
                            <label><b>No Faktur :{{ $transaksi->id_transaksi }}</b></label>
                                <span></span>
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-success" id="add_barang">Tambah Barang</button>
                        </div>
                        <div class="row">
                            <h3>List Barang</h3>
                        </div>
                        <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ID Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Harga Satuan</th>
                                        <th>Jumlah</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $total_bayar = 0; @endphp
                                    @foreach($dbarang as $row2)
                                    @php $total_harga =  $row2->harga_barang * $row2->jumlah;
                                         $total_bayar += $total_harga;
                                    @endphp
                                    <tr>
                                <td>{{ $row2->id_barang }}</td>
                                <td>{{ $row2->nama_barang }}</td>
                                <td>{{ $row2->harga_barang }}</td>
                                <td>{{ $row2->jumlah }}</td>
                                <td>{{ $total_harga }}</td>
                            </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <form action="/transaksi/bayar" method="POST">
                            {{ csrf_field() }}
                        <div class="row">
                            <label>Total Bayar: </label>
                        <input type="number" class="form-control" name="total_bayar"  readonly id="total_bayar" value="{{ $total_bayar }}" >
                        </div>
                        <div class="row">
                            <label>Nominal Bayar</label>
                            <input type="number" class="form-control" name="jumlah_bayar" id="jumlah_bayar"  onkeyup="bayar();" onchange="bayar();">
                        </div>
                        <div class="row">
                            <label>Kembalian</label>
                            <input type="number" class="form-control" name="kembalian" readonly id="kembalian">
                        </div>
                        <div class="row">
                            <button class="btn btn-success">Bayar</button>
                        </div>
                        </form>
                    </div>


                  </div>

            </div>
        </div>
<script>
    function bayar(){
    var total_bayar =  parseInt(document.getElementById("total_bayar").value);
    var jumlah_bayar = parseInt(document.getElementById("jumlah_bayar").value);
    var kembalian = jumlah_bayar - total_bayar;
    document.getElementById("kembalian").value = kembalian;
}
</script>
    </body>
@endsection

</html>
