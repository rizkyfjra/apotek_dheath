@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item">Tes</li>
            <li class="breadcrumb-item active">Tis</li>
        </ol>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
        @endif
        <!-- Page Content -->
        <div class="row">
            <div class="col-md-12 table-responsive my-3">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Produk</th>
                            <th>Stok</th>
                        </tr>
                    </thead>
                    <tbody>
                        <td width="50%">{{ $info[0]->nama_produk }}</td>
                        <td width="50%">{{ $info[0]->stok }}</td>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <form action="{{ url('transaksi')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="id_produk" class="control-label">Produk</label>
                        <select name="id_produk" id="id_produk" class="form-control">
                            <option selected disabled hidden value="">--PILIH--</option>
                            @foreach ($produk as $row)
                            <option value="{{ $row->id }}">{{ $row->nama_produk }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stok_keluar" class="control-label">Stok Keluar</label>
                        <input type="number" id="stok_keluar" class="form-control" name="stok_keluar">
                    </div>
                    <div class="form-group">
                        <input type="submit" id="stok_keluar" class="form-control btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.table').DataTable();
});
</script>
@endsection
