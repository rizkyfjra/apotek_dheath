@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Klinik</li>
        </ol>

        <!-- Page Content -->
        <!-- Icon Cards-->
        <a class="btn btn-success btn-sm" href="javascript:void(0)" id="create"><i class="fas fa-wd fa-plus"></i> Tambah Data Klinik</a>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-hover w-100">
                <thead class="thead-light">
                    <tr>
                        <th scope="col" width="5%">No</th>
                        <th scope="col">Nama Klinik</th>
                        <th scope="col">Alamat</th>
                        <th scope="col" width="12%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modalHeader"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="form" name="form" class="form-horizontal">
            <div class="modal-body">
                <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="klinik" class="control-label">Nama Klinik</label>
                        <input type="text" class="form-control" id="klinik" name="klinik" placeholder="Nama klinik" autocomplete="off">
                        <span id="klinik-error" class="help-block text-danger p-1"></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="alamat">Alamat</label>
                        <textarea id="alamat" name="alamat" placeholder="Alamat Klinik" class="form-control"></textarea>
                        <span id="alamat-error" class="help-block text-danger p-1"></span>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
            </div>
        </form>
        </div>
    </div>
</div>
<script>
    $(function() {
        // Set Up header, token CSRF apalah
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Set Data Table
        var table = $('.table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('klinik.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'klinik', name:'klinik'},
                {data: 'alamat', name:'alamat'},
                {data: 'action', name:'action', orderable: false, searchable: false}
            ]
        });
        // Trigger form create
        $('#create').click(function(){
            $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
            $('#id').val('');
            $('#form').trigger('reset');
            $('#modalHeader').text('Tambah Data Klinik');
            $('#modal').modal('show');
            $('.help-block').empty();
        });
        // Trigger form edit
        $('body').on('click', '.editData', function () {
            var id = $(this).data('id');
            $.get("{{ route('klinik.index') }}"+'/'+id+'/edit', function(data) {
                $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                $('#id').val(data.id);
                $('#modalHeader').text('Sunting Data '+data.klinik);
                $('#modal').modal('show');
                $('.help-block').empty();
                $('#klinik').val(data.klinik);
                $('#alamat').val(data.alamat);
            })
        });
        // Proses Create dan Update
        $('#saveBtn').click(function (e) { 
            e.preventDefault();
            var klinik = $('#klinik').val();
            var alamat = $('#alamat').val();
            $(this).html('<i class="fa fa-wd fa-clock-o"></i> Memproses..');
            $(this).attr('disabled',true);
            if(klinik.length < 1 || alamat.length < 1) {
                validasi_klinik();
                validasi_alamat();
                $(this).html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                $(this).attr('disabled',false);
            }
            else
            {
            $.ajax({
                data: $('#form').serialize(),
                url: "{{ route('klinik.store') }}",
                type: "POST",
                beforeSend :function () {
                    swal({
                        title: 'Menunggu',
                        html: 'Memproses data',
                        onOpen: () => {
                        swal.showLoading()
                        }
                    })      
                },
                dataType: 'json',
                success: function(data) {
                        swal({
                            title: "Berhasil!",
                            text: data.success,
                            type: "success"
                        });
                        $('#form').trigger('reset');
                        $(this).html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                        $(this).attr('disabled',false);
                        $('#modal').modal('hide');
                        table.draw();
                    }
                });
            }
        });
        // hapus data
        $('body').on('click', '.deleteData', function () {
            var id = $(this).data('id');
            swal({
                title: "Apa Anda yakin untuk Menghapus Data ini?",
                text: "Data tidak dapat Dikembalikan setelah Terhapus",
                type: "error",
                showCancelButton: true,
                cancelButtonClass: '#DD6B55',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Hapus',
            }).then(function (e) {
                if(e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'DELETE',
                        url: "{{ url('klinik') }}"+'/'+id,
                        data: {_token: CSRF_TOKEN},
                        beforeSend :function () {
                            swal({
                                title: 'Menunggu',
                                html: 'Memproses data',
                                onOpen: () => {
                                swal.showLoading()
                                }
                            })      
                        }, 
                        dataType: 'JSON',
                        success: function(data) {
                            swal(
                                'Berhasil',
                                'Data Berhasil Dihapus!',
                                'success'
                            );
                            table.draw();
                        }
                    })
                }
                else
                {
                    swal(
                        'Batal',
                        'Anda membatalkan penghapusan',
                        'error'
                    );
                }
            })
        })
        // validasi klinik
        function validasi_klinik() {
            var panjang = $("#klinik").val().length;
            if(panjang < 1){
                $("#klinik-error").html("Nama Klinik harus Diisi");
                $("#klinik-error").show().addClass("error");
                error_klinik = true;
            }else{
                $("#klinik-error").hide();
            }
        }
        // validasi alamat
        function validasi_alamat() {
            var panjang = $("#alamat").val().length;
            if(panjang < 1){
                $("#alamat-error").html("Alamat Kalimat harus Diisi");
                $("#alamat-error").show().addClass("error");
                error_alamat = true;
            }else{
                $("#alamat-error").hide();
            }
        }
    });
</script>
@endsection