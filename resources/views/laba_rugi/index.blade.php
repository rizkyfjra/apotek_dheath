@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Laba Rugi</li>
        </ol>
        <h4>Neraca Laba Rugi</h4>
        <hr>
        <div class="row my-2">
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" id="harga" class="form-control" placeholder="Harga Jual">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" id="modal" class="form-control" placeholder="Modal">
                </div>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-2" align="center">
                <button type="button" class="btn btn-success btn-block btn-md" id="hitung"><i class="fas fa-balance-scale"></i> Hitung</button>
            </div>
            <div class="col-md-2" align="center">
                <button type="button" class="btn btn-secondary btn-block btn-md" id="reset"><i class="fas fa-redo"></i> Reset</button>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-12" align="center">
                <div id="hasil"></div>
            </div>
        </div>
    </div>
</div>
<script>
    var harga = document.getElementById('harga');
    harga.addEventListener('keyup', function(e){
        harga.value = formatRupiah(this.value, '');
    });
    var modal = document.getElementById('modal');
    modal.addEventListener('keyup', function(e){
        modal.value = formatRupiah(this.value, '');
    });
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
        if(ribuan)
        {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
    $(document).ready(function () {
        // separator
        function separator(kalimat) {
            return kalimat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
        // operasi perhitungan laba dan rugi
        $('#hitung').click(function (e) { 
            e.preventDefault();
            var harga = $('#harga').val().split('.').join('');
            var modal = $('#modal').val().split('.').join('');
            if(harga == '' || modal == '')
            {
                swal({
                    title: "Peringatan!",
                    text: 'Mohon Masukan Nilai Harga dan Modal!',
                    type: "warning"
                });
            }
            else
            {    
                if(harga > modal) {
                    var laba = harga - modal;
                    $('#hasil').html('<h2>Anda Mendapatkan Laba Sebesar Rp. '+separator(laba)+'</h2>');
                }
                else if(modal > harga)
                {
                    var rugi = modal - harga;
                    $('#hasil').html('<h2>Anda Mendapatkan Kerugian Sebesar Rp. '+separator(rugi)+'</h2>');
                }
                else
                {
                    $('#hasil').html('<h2>Anda Mendapatkan Kerugian Sebesar tidak Mendapatkan Laba atau pun Rugi</h2>');
                }
            }
        });
        // reset button
        $('#reset').click(function (e) { 
            e.preventDefault();
            $('#harga').val('');
            $('#modal').val('');
        });
        // $('#harga').keyup(function (e) { 
        //     var harga = $(this).val().split('.').join('');
        //     var modal = $('#modal').val().split('.').join('');
        //     if(harga = '')
        //     {
        //         var harga = 0;
        //     }
        //     if(modal = '')
        //     {
        //         var modal = 0;
        //     }
        //     if(harga > modal) {
        //         var laba = harga - modal;
        //         $('#hasil').html('<h2>Anda Mendapatkan Laba Sebesar Rp. '+separator(laba)+'</h2>');
        //     }
        //     else
        //     {
        //         var rugi = modal - harga;
        //         $('#hasil').html('<h2>Anda Mendapatkan Kerugian Sebesar Rp. '+separator(rugi)+'</h2>');
        //     }
        // });
        // $('#modal').keyup(function (e) { 
        //     var harga = $('#harga').val().split('.').join('');
        //     var modal = $(this).val().split('.').join('');
        //     if(harga = '')
        //     {
        //         var harga = 0;
        //     }
        //     if(modal = '')
        //     {
        //         var modal = 0;
        //     }
        //     if(harga > modal) {
        //         var laba = harga - modal;
        //         $('#hasil').html('<h2>Anda Mendapatkan Laba Sebesar Rp. '+separator(laba)+'</h2>');
        //     }
        //     else
        //     {
        //         var rugi = modal - harga;
        //         $('#hasil').html('<h2>Anda Mendapatkan Kerugian Sebesar Rp. '+separator(rugi)+'</h2>');
        //     }
        // });
    });
</script>
@endsection