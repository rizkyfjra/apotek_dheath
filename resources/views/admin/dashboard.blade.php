@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <div class="row">
            <div class="col-xl-3 col-sm-6 mb-3">
                <div class="card text-white bg-primary o-hidden h-100">
                    <div class="card-body">
                        <div class="card-body-icon">
                            <i class="fas fa-fw fa-shopping-cart"></i>
                        </div>
                        <div class="mr-5">RESEP</div>
                    </div>
                </div>
            </div>
            
        </div>

    </div>

    @endsection
