@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item">Master Data</li>
            <li class="breadcrumb-item active">Produk</li>
        </ol>

        <!-- Page Content -->
        <!-- Icon Cards-->
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
        @endif
        <a class="btn btn-success btn-sm" href="javascript:void(0)" id="create"><i class="fas fa-wd fa-plus"></i> Tambah Data Produk</a>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-hover w-100">
                <thead class="thead-light">
                    <tr>
                        <th scope="col" width="5%">No</th>
                        <th scope="col" width="5%">Gambar</th>
                        <th scope="col">Produk</th>
                        <th scope="col">Jenis Obat</th>
                        <th scope="col">Kategori</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Stok</th>
                        <th scope="col" width="8%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modalHeader"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="form" name="form" class="form-horizontal" enctype="multipart/form-data" method="POST">
            <div class="modal-body">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div id="metode">
                    @method('patch')
                </div>
                <div class="form-group">
                    <label for="nama_produk" class="control-label">Nama Produk</label>
                    <input type="text" class="form-control" id="nama_produk" name="nama_produk" placeholder="Nama Produk" autocomplete="off">
                    <span id="nama_produk-error" class="help-block text-danger p-1"></span>
                </div>
                <div class="form-group">
                    <label for="id_jenis_obat" class="control-label">Jenis Obat</label>
                    <select name="id_jenis_obat" id="id_jenis_obat" class="form-control">
                        <option selected disabled hidden value="">--PILIH--</option>
                        @foreach ($jenis as $row)
                        <option value="{{ $row->id }}">{{ $row->jenis_obat }}</option>
                        @endforeach
                    </select>
                    <span id="id_jenis_obat-error" class="help-block text-danger p-1"></span>
                </div>
                <div class="form-group">
                    <label for="id_kategori" class="control-label">Kategori Obat</label>
                    <select name="id_kategori" id="id_kategori" class="form-control">
                        <option selected disabled hidden value="">--PILIH--</option>
                        @foreach ($kategori as $row)
                            <option value="{{ $row->id }}">{{ $row->kategori }}</option>
                        @endforeach
                    </select>
                    <span id="id_kategori-error" class="help-block text-danger p-1"></span>
                </div>
                <div class="form-group">
                    <label for="harga" class="control-label">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga" placeholder="Harga Produk" autocomplete="off">
                    <span id="harga-error" class="help-block text-danger p-1"></span>
                </div>
                <div class="form-group">
                    <label for="foto-obat" class="control-label">Foto Produk</label>
                    <small id="ket_foto" class="text-danger">*Biarkan kosong jika tidak ingin diganti</small>
                    <input type="file" class="form-control-file dropify" id="foto_obat" name="foto_obat" 
                    data-max-file-size="5M" data-allowed-file-extensions="jpg jpeg png">
                </div>
                <div id="foto_lama"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
                <button type="button" class="btn btn-success" id="validasi"><i class="fas fa-exclamation"></i> Validasi</button>
                <button type="submit" class="btn btn-primary" id="submit">Save changes</button>
            </div>
        </form>
        </div>
    </div>
</div>
<script>
    // untuk menentukan apa ini create atau update
    var save_method;
    // untuk membuat kolom harga hanya menerima input angka beserta separator
    var harga = document.getElementById('harga');
    harga.addEventListener('keyup', function(e){
        harga.value = formatRupiah(this.value, '');
    });
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
        if(ribuan)
        {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
    $(function() {
        // Set Up header, token CSRF apalah
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Set Data Table
        var table = $('.table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('produk.index') }}",
            aLengthMenu: [[5, 10, 50, 75, -1], [5, 10, 50, 75, "All"]],
            pageLength: 5,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'image', name:'image'},
                {data: 'nama_produk', name:'nama_produk'},
                {data: 'jenis_obat', name:'jenis_obat'},
                {data: 'kategori', name:'kategori'},
                {data: 'harga', name:'harga'},
                {data: 'stok', name:'stok'},
                {data: 'action', name:'action', orderable: false, searchable: false}
            ]
        });
        // set dropify
        $('.dropify').dropify();
        // Trigger form create
        $('#create').click(function(){
            $('#submit').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
            $("#submit").hide();
            $('#id').val('');
            $('#form').trigger('reset');
            $('#form').attr('action', "{{ route('produk.store') }}");
            $('#modalHeader').text('Tambah Data Produk');
            $('#modal').modal('show');
            $('input[name = _method]').val('post');
            $('.help-block').hide();
            $('#ket_foto').hide();
            $('#foto_lama').hide();
        });
        // Trigger form edit
        $('body').on('click', '.editData', function () {
            var id = $(this).data('id');
            $.get("{{ route('produk.index') }}"+'/'+id+'/edit', function(data) {
                $('#submit').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                $("#submit").hide();
                $('#id').val(data.id);
                $('#form').attr('action', "{{ url('produk') }}"+'/'+id);
                $('#modalHeader').text('Sunting Data '+data.nama_produk);
                $('#modal').modal('show');
                $('.help-block').hide();
                if(data.foto_obat)
                {
                    $('#ket_foto').show();
                }
                else
                {
                    $('#ket_foto').hide();
                    $('#foto_lama').hide();
                }
                $('#metode').show();
                $('#nama_produk').val(data.nama_produk);
                $('#id_jenis_obat').val(data.id_jenis_obat);
                $('#id_kategori').val(data.id_kategori);
                $('#harga').val(formatRupiah(data.harga, ''));
            })
        });
        // Proses Create dan Update
        $('#validasi').click(function (e) { 
            e.preventDefault();
            $(this).html('<i class="fa fa-wd fa-clock-o"></i> Memproses..');
            $(this).attr('disabled',true);
            var url;
            var id              = $('#id').val();
            var nama_produk     = $('#nama_produk').val();
            var id_jenis_obat   = $('select[name=id_jenis_obat] option').filter(':selected').val();
            var id_kategori     = $('select[name=id_kategori] option').filter(':selected').val();
            var harga           = $('#harga').val();
            var foto_obat       = $('#foto_obat').val();
            if(nama_produk.length < 1 || id_jenis_obat == '' || id_kategori == '' || harga.length < 1)
            {
                if(nama_produk.length < 1)
                {
                    $("#nama_produk-error").html("Nama Produk harus Diisi!");
                    $("#nama_produk-error").show().addClass("error");
                    error_nama_produk = true;
                    $("#validasi").html('<i class="fas fa-exclamation"></i> Validasi');
                    $("#validasi").attr('disabled',false);
                }
                else
                {
                    $("#nama_produk-error").hide();
                }
                if(id_jenis_obat == '')
                {
                    $("#id_jenis_obat-error").html("Mohon Pilih Jenis Obat!");
                    $("#id_jenis_obat-error").show().addClass("error");
                    error_id_jenis_obat = true;
                    $("#validasi").html('<i class="fas fa-exclamation"></i> Validasi');
                    $("#validasi").attr('disabled',false);
                }
                else
                {
                    $("#id_jenis_obat-error").hide();
                }
                if(id_kategori == '')
                {
                    $("#id_kategori-error").html("Mohon Pilih Kategori Obat!");
                    $("#id_kategori-error").show().addClass("error");
                    error_id_kategori = true;
                    $("#validasi").html('<i class="fas fa-exclamation"></i> Validasi');
                    $("#validasi").attr('disabled',false);
                }
                else
                {
                    $("#id_kategori-error").hide();
                }
                if(harga.length < 1)
                {
                    $("#harga-error").html("Nama Produk harus Diisi!");
                    $("#harga-error").show().addClass("error");
                    error_harga = true;
                    $("#validasi").html('<i class="fas fa-exclamation"></i> Validasi');
                    $("#validasi").attr('disabled',false);
                }
                else
                {
                    $("#harga-error").hide();
                }
            }
            else
            {
                swal({
                    title: "Validasi Berhasil!",
                    text: 'Silahkan Tekan Tombol Submit',
                    type: "success"
                });
                $("#nama_produk-error").hide();
                $("#id_jenis_obat-error").hide();
                $("#id_kategori-error").hide();
                $("#harga-error").hide();
                $(this).hide();
                $("#submit").show();
            }
        });
        // hapus data
        $('body').on('click', '.deleteData', function () {
            var id = $(this).data('id');
            swal({
                title: "Apa Anda yakin untuk Menghapus Data ini?",
                text: "Data tidak dapat Dikembalikan setelah Terhapus",
                type: "error",
                showCancelButton: true,
                cancelButtonClass: '#DD6B55',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Hapus',
            }).then(function (e) {
                if(e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'DELETE',
                        url: "{{ url('produk') }}"+'/'+id,
                        data: {_token: CSRF_TOKEN},
                        beforeSend :function () {
                            swal({
                                title: 'Menunggu',
                                html: 'Memproses Data',
                                onOpen: () => {
                                swal.showLoading()
                                }
                            })      
                        }, 
                        dataType: 'JSON',
                        success: function(data) {
                            swal(
                                'Berhasil',
                                'Data Berhasil Dihapus!',
                                'success'
                            );
                            table.draw();
                        }
                    })
                }
                else
                {
                    swal(
                        'Batal',
                        'Anda membatalkan penghapusan',
                        'error'
                    );
                }
            })
        });
        // validasi Kategori
        // function validasi_kategori() {
        //     var panjang     = $("#kategori").val().length;
        //     var kategori    = $('#kategori').val();
        //     var id          = $('#id').val();
        //     if(panjang < 1){
        //         $("#kategori-error").html("Kategori harus Diisi");
        //         $("#kategori-error").show().addClass("error");
        //         error_kategori = true;
        //         $("#saveBtn").html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
        //         $("#saveBtn").attr('disabled',false);
        //     }
        //     else
        //     {
        //         $.ajax({
        //             url: "{{ url('get-kategori') }}",
        //             data: {kategori:kategori, id:id},
        //             beforeSend :function () {
        //                     swal({
        //                         title: 'Menunggu',
        //                         html: 'Memvalidasi Data',
        //                         onOpen: () => {
        //                         swal.showLoading()
        //                         }
        //                     })      
        //                 }, 
        //             dataType: 'JSON',
        //             success: function(data) {
        //                 if(data.length > 0) {
        //                     swal.close();
        //                     $("#kategori-error").html("Kategori "+kategori+" sudah Digunakan");
        //                     $("#kategori-error").show().addClass("error");
        //                     error_kategori = true;
        //                     $("#saveBtn").html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
        //                     $("#saveBtn").attr('disabled',false);
        //                 }
        //                 else
        //                 {
        //                     $("#kategori-error").hide();
        //                     proses();
        //                 }
        //             }
        //         })
        //     }
        // }
    });
</script>
@endsection