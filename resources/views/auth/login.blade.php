<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Log In</title>

<!-- Custom fonts for this template-->
<link href="lte/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- Custom styles for this template-->
<link href="lte/css/sb-admin.css" rel="stylesheet">

</head>

<body class="bg-muted">

    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header bg-info">
                <h3 class="text-white">{{ __('Login') }}.</h3>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <div class="form-label-group">
                            <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required  autofocus>
                            {{-- <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email" autofocus> --}}
                            <label for="inputEmail">Username atau Alamat Email</label>
                            @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="current-password">
                            <label for="inputPassword">Password</label>

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info btn-block">
                        {{ __('Login') }}
                    </button>
                    <hr>
                </form>
                <div class="text-center">
                    <a class="d-block small mt-3" href="{{ route('register') }}">Tambah Akun</a>
                    {{-- <a class="d-block small" href="{{ route('password.request') }}">Lupa Password?</a> --}}
                </div>
            </div>
        </div>
    </div>
    <footer class="page-footer font-small blue">
        <div class="footer-copyright text-center py-3">TEST WEB DEVELOPER <b>d'health</b> <br> Rizky Fajar Anugrah 
        {{ date('Y') }}
        </div>
    </footer>
    <script src="lte/vendor/jquery/jquery.min.js"></script>
    <script src="lte/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="lte/vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
