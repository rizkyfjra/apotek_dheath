@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Stok Obat Alkes</li>
        </ol>

        <!-- Page Content -->
        <!-- Icon Cards-->
        <a type="button" class="btn btn-outline-primary btn-sm" href="javascript:void(0)" id="create">Tambah Stok</a>
        <a type="button" class="btn btn-outline-warning btn-sm" href="stokobat/retur">Retur Stok Obat Alkes</a>
        <hr />

        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Jenis Kelompok</th>
                    <th scope="col">Jenis</th>
                    <th scope="col">Kode Obat</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Tanggal Kadaluarsa</th>
                    <th scope="col">Stok Masuk</th>
                    <th scope="col">Stok Keluar</th>
                    <th scope="col">Stok Akhir</th>
                    <th scope="col" width="12%">Aksi</th>
                </tr>
            </thead>
            <tbody>
                </tbody>
        </table>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modalHeader"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="form" name="form" class="form-horizontal">
            <div class="modal-body">
                <input type="text" name="stokobatalkes_id" id="stokobatalkes_id" >
                    <div class="form-group">
                        <label for="klinik" class="control-label">Jenis Kelompok Obat</label>
                        <input type="text" class="form-control" id="jeniskelompok" name="jeniskelompok" placeholder="Jenis Kelompok Obat" autocomplete="off">
                        <span id="klinik-error" class="help-block text-danger p-1"></span>
                    </div>
                    <div class="form-group">
                        <label for="jenis_obatalkes" class="control-label">Jenis Obat Alkes</label>
                        <input type="text" class="form-control" id="jenis_obatalkes" name="jenis_obatalkes" placeholder="Jenis Obat Alkes" autocomplete="off">
                        <span id="klinik-error" class="help-block text-danger p-1"></span>
                    </div>
                    <div class="form-group">
                        <label for="kode_obat" class="control-label">Kode Obat</label>
                        <input type="text" class="form-control" id="kode_obat" name="kode_obat" placeholder="Kode Obat" autocomplete="off">
                        <span id="kode_obat-error" class="help-block text-danger p-1"></span>
                    </div>
                    <div class="form-group">
                        <label for="nama_stokobatalkes" class="control-label">Nama Obat</label>
                        <input type="text" class="form-control" id="nama_stokobatalkes" name="nama_stokobatalkes" placeholder="Nama Obat" autocomplete="off">
                        <span id="nama_stokobatalkes-error" class="help-block text-danger p-1"></span>
                    </div>
                    <div class="form-group">
                        <label for="kategori" class="control-label">Kategori</label>
                        <input type="text" class="form-control" id="kategori" name="kategori" placeholder="Kategori" autocomplete="off">
                        <span id="kategori-error" class="help-block text-danger p-1"></span>
                    </div>
                    <div class="form-group">
                        <label for="tanggal_kadaluarsa" class="control-label">Tanggal Kadaluarsa</label>
                        <input type="date" class="form-control" id="tanggal_kadaluarsa" name="tanggal_kadaluarsa" placeholder="Tanggal Kadaluarsa" autocomplete="off">
                        <span id="tanggal_kadaluarsa-error" class="help-block text-danger p-1"></span>
                    </div>
                    <div class="form-group">
                        <label for="stok_masuk" class="control-label">Jumlah Stok Masuk</label>
                        <input type="number" class="form-control" id="stok_masuk" name="stok_masuk" placeholder="Jumlah Stok Masuk" autocomplete="off">
                        <span id="stok_masuk-error" class="help-block text-danger p-1"></span>
                    </div>
                    <div class="form-group">
                        <label for="stok_keluar" class="control-label">Jumlah Stok Keluar</label>
                        <input type="number" class="form-control" id="stok_keluar" name="stok_keluar" placeholder="Jumlah Stok Keluar" autocomplete="off">
                        <span id="stok_keluar-error" class="help-block text-danger p-1"></span>
                    </div>
                    <div class="form-group">
                        <label for="stok_akhir" class="control-label">Jumlah Stok Akhir</label>
                        <input type="number" class="form-control" id="stok_akhir" name="stok_akhir" placeholder="Jumlah Stok Akhir" autocomplete="off">
                        <span id="stok_akhir-error" class="help-block text-danger p-1"></span>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label" for="alamat">Alamat</label>
                        <textarea id="alamat" name="alamat" placeholder="Alamat Klinik" class="form-control"></textarea>
                        <span id="alamat-error" class="help-block text-danger p-1"></span>
                    </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
            </div>
        </form>
        </div>
    </div>
</div>


        <script>
            $(document).ready(function () {
                // Set Up header, token CSRF apalah
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                // Set Data Table
                var table = $('.table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('stokobat.index') }}",
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                        {data: 'jeniskelompok', name:'jeniskelompok'},
                        {data: 'jenis_obatalkes', name:'jenis_obatalkes'},
                        {data: 'kode_obat', name:'kode_obat'},
                        {data: 'nama_stokobatalkes', name:'nama_stokobatalkes'},
                        {data: 'kategori', name:'kategori'},
                        {data: 'tanggal_kadaluarsa', name:'tanggal_kadaluarsa'},
                        {data: 'stok_masuk', name:'stok_masuk'},
                        {data: 'stok_keluar', name:'stok_keluar'},
                        {data: 'stok_akhir', name:'stok_akhir'},
                        {data: 'action', name:'action', orderable: false, searchable: false}
                    ]
                });
            });

             // Trigger form create
        $('#create').click(function(){
            $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
            $('#stokobatalkes_id').val('stokobatalkes_id');
            $('#form').trigger('reset');
            $('#modalHeader').text('Tambah Data Stok Obat Alkes');
            $('#modal').modal('show');
            $('.help-block').empty();
        });
         // Trigger form edit
         $('body').on('click', '.editData', function () {
            var stokobatalkes_id = $(this).data('stokobatalkes_id');
            $.get("{{ route('stokobat.index') }}"+'/'+stokobatalkes_id+'/edit', function(data) {
                $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                $('#stokobatalkes_id').val(data.stokobatalkes_id);
                $('#modalHeader').text('Sunting Data '+data.nama_stokobatalkes);
                $('#modal').modal('show');
                $('.help-block').empty();
                $('#jeniskelompok').val(data.jeniskelompok);
                $('#jenis_obatalkes').val(data.jenis_obatalkes);
                $('#kode_obat').val(data.kode_obat);
                $('#nama_stokobatalkes').val(data.nama_stokobatalkes);
                $('#kategori').val(data.kategori);
                $('#tanggal_kadaluarsa').val(data.tanggal_kadaluarsa);
                $('#stok_masuk').val(data.stok_masuk);
                $('#stok_keluar').val(data.stok_keluar);
                $('#stok_akhir').val(data.stok_akhir);
            })
        });
        // Proses Create dan Update
        $('#saveBtn').click(function (e) { 
            e.preventDefault();
            var stokobatalkes_id = $('#stokobatalkes_id').val();
            var jeniskelompok = $('#jeniskelompok').val();
            var jenis_obatalkes = $('#jenis_obatalkes').val();
            var kode_obat = $('#kode_obat').val();
            var nama_stokobatalkes = $('#nama_stokobatalkes').val();
            var kategori = $('#kategori').val();
            var tanggal_kadaluarsa = $('#tanggal_kadaluarsa').val();
            var stok_masuk = $('#stok_masuk').val();
            var stok_keluar = $('#stok_keluar').val();
            var stok_akhir = $('#stok_akhir').val();
            var alamat = $('#alamat').val();
            $(this).html('<i class="fa fa-wd fa-clock-o"></i> Memproses..');
            $(this).attr('disabled',true);
            if(nama_stokobatalkes.length < 1 || jeniskelompok.length < 1) {
                validasi_nama_stokobatalkes();
                validasi_jeniskelompok();
                $(this).html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                $(this).attr('disabled',false);
            }
            else
            {
            $.ajax({
                data: $('#form').serialize(),
                url: "{{ route('stokobat.store') }}",
                type: "POST",
                beforeSend :function () {
                    swal({
                        title: 'Menunggu',
                        html: 'Memproses data',
                        onOpen: () => {
                        swal.showLoading()
                        }
                    })      
                },
                dataType: 'json',
                success: function(data) {
                        swal({
                            title: "Berhasil!",
                            text: data.success,
                            type: "success"
                        });
                        $('#form').trigger('reset');
                        $(this).html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                        $(this).attr('disabled',false);
                        $('#modal').modal('hide');
                        table.draw();
                    }
                });
            }
              // validasi nama stokobat
        function validasi_nama_stokobatalkes() {
            var panjang = $("#nama_stokobatalkes").val().length;
            if(panjang < 1){
                $("#nama_stokobatalkes-error").html("Nama Klinik harus Diisi");
                $("#nama_stokobatalkes-error").show().addClass("error");
                error_nama_stokobatalkes = true;
            }else{
                $("#nama_stokobatalkes-error").hide();
            }
        }
        // validasi jenis kelompok
        function validasi_jeniskelompok() {
            var panjang = $("#jeniskelompok").val().length;
            if(panjang < 1){
                $("#jeniskelompok-error").html("Alamat Kalimat harus Diisi");
                $("#jeniskelompok-error").show().addClass("error");
                error_jeniskelompok = true;
            }else{
                $("#jeniskelompok-error").hide();
            }
        }
        });
        </script>
        @endsection