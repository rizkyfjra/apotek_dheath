@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item">Transaksi</li>
            <li class="breadcrumb-item active">Umum</li>
        </ol>

        <!-- Page Content -->
        <div class="row">
            <div class="col-md-6 stretch-card grid-margin">
                <div class="card shadow" style="height: 47rem;">
                    <div class="card-body" style="overflow-y: scroll;">
                        <nav class="nav nav-tabs" id="nav-tab" role="tablist">
                            <?php $i = 0; foreach ($kategori as $k) : ?>  
                                    <a class="nav-item nav-link mr-2 <?= ($i == 0) ? 'active' : '' ?> font-weight-bold" 
                                        style="border-radius: 7px; border: solid;" id="nav-home-tab<?= $k->kategori ?? str_replace(' ', '', $k->kategori) ?>" 
                                        data-toggle="tab" href="#nav-home-<?= str_replace(' ', '', $k->kategori) ?>" role="tab" 
                                        aria-controls="nav-home<?= str_replace(' ', '', $k->kategori) ?>" aria-selected="true">
                                        <h3><?= $k->kategori ?></h3>
                                    </a>
                            <?php $i++; endforeach; ?>
                        </nav>
                        <hr>
                        <div class="tab-content" id="nav-tabContent">
                            <?php 
                            $c = 0; foreach ($kategori as $a) : 
                            $kat = DB::table('obatalkes_m')
                            ->where('obatalkes_m.id_kategori', '=', $a->id)
                            ->select('obatalkes_m.*')
                            ->get();
                            ?>
                            <div class="tab-pane fade show <?= ($c == 0) ? 'active' : '' ?>" id="nav-home-<?= str_replace(' ', '', $a->kategori) ?>" 
                                role="tabpanel" aria-labelledby="nav-home-tab<?= str_replace(' ', '', $a->kategori) ?>">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xl-12 mb-2">
                                            <input type="text" class="form-control search" autocomplete="off">
                                        </div>
                                       <?php $j = 0; $k = 0; foreach ($kat as $t) : 
                                        $stok = DB::table('obatalkes_m')
                                                ->where('obatalkes_m.obatalkes_id', $t->obatalkes_id)
                                                ->select('obatalkes_m.stok')
                                                ->get();
                                        $warna = ['info', 'primary', 'warning', 'success'];
                                            if ($k > 3) {
                                                $modulus = ($k % 3);
                                                $wr = $modulus;
                                            } else {
                                                $wr = $k;
                                            }
                                        ?>
                                        @if ($stok[0]->stok > 0)
                                            <div class="col-md-3 menu-card" id="menu-{{ $t->obatalkes_id }}">
                                                <div class="card text-white bg-<?= $warna[$wr] ?> text-center nm_product shadow card-hover pulse" 
                                                    style="height: 15rem; cursor: pointer; border-radius: 20px;" data-id="{{ $t->obatalkes_id }}" 
                                                    nama-produk="{{ $t->obatalkes_nama }}" data-stok="{{ $stok[0]->stok }}">
                                                    <div class="card-body">
                                                        <h6 class="text-white mb-2 nama-product" data-id="{{ $t->obatalkes_id }}">{{ $t->obatalkes_nama }}</h6>
                                                        <hr>
                                                        <h6 class="card-text">{{ $t->stok }} Stok</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-3 menu-card" id="menu-{{ $t->obatalkes_id }}">
                                                <div class="card text-white bg-{{ $warna[$wr] }} text-center shadow card-hover pulse" 
                                                    style="height: 15rem; cursor: pointer; border-radius: 20px; opacity: 0.6;" data-id="{{ $t->obatalkes_id }}" 
                                                    nama-produk="{{ $t->obatalkes_nama }}" data-stok="{{ $stok[0]->stok }}">
                                                    <div class="card-body">
                                                        <h6 class="text-white mb-2 nama-product" data-id="{{ $t->obatalkes_id }}">{{ $t->obatalkes_nama }}</h6>
                                                        <hr>
                                                        <h6 class="card-text">{{ $t->stok }} Stok</h6>
                                                        <b class="card-text" style="font-size: 12px">Stok Tidak Tersedia</b>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <?php $j++; $k++; $wr--; endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <?php $c++; endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 stretch-card grid-margin">
                <div class="card shadow" style="height: 47rem;">
                    <div class="card-header">
                        <h5 class="text-title">List Resep</h5>
                    </div>
                    <div class="card-body p-0 m-0">
                        <div class="table-responsive">
                            <table id="tabel_pesanan" class="table table-bordered table-hover w-100">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Produk</th>
                                        <th scope="col">Kode</th>
                                        <th scope="col">Qty</th>
                                        <th scope="col">Diskon</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-header shadow" style="height: 250px;">
                        <table class="table table-no-border mb-2" id="tabel-bawah">
                            <tbody>
                                
                                <tr class="font-weight-bold">
                                    <td style="font-size: 18px;"></td>
                                    <td class="text-right">
                                        <div class="row">
                                            <div class="col-md-10 offset-md-2 easy-get4" data-id="tunai">
                                                <input type="hidden" style="font-size: 18px;"  class="form-control input text-right angka" name="tunai" id="tunai" value="20">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <button class="btn-block btn btn-lg btn-success btn-fw" style="font-size: 18px;" id="btn_transaksi" hidden>SIMPAN TRANSAKSI RESEP</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Edit Produk -->
<div class="modal fade" id="modal_ubah_list" role="dialog" aria-labelledby="exampleModalCenterTitle2" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered w-50" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #faa307;">
          <h5 class="modal-title font-weight-bold text-white judul">Product</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="mr-2 text-white">&times;</span>
          </button>
        </div>
        <form id="form_list" autocomplete="off">
              <input type="hidden" name="id" id="id">
              <div class="modal-body">
                  <div class="col-md-8 offset-md-2 pt-2">
                      <div class="form-group row">
                          <label class="col-sm-3 col-form-label">QTY</label>
                          <div class="col-sm-5 easy-get2" data-id="jumlah">
                              <input type="text" class="form-control angka text-center"  style="font-size: 14px;" name="jumlah" id="jumlah" placeholder="Qty">
                          </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Diskon</label>
                          <div class="col-sm-8 easy-get3" data-id="nilai_diskon">
                              <input type="text" class="form-control angka text-right" style="font-size: 14px;" name="nilai_diskon" id="nilai_diskon" placeholder="Masukkan Diskon">
                          </div>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                  <button type="button" style="background-color: #faa307;" class="btn text-white" id="simpan_produk">Simpan</button>
              </div>
          </form>
      </div>
    </div>
  </div>
  
  <!-- Modal Pelanggan -->
  <div class="modal fade" id="modal_info_pelanggan" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header" style="background-color: #faa307;">
              <h5 class="modal-title font-weight-bold text-white">Informasi Pelanggan</h5>
              <button type="button" class="close close-menu-transaksi" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" class="mr-2 text-white">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="form-group">
                  <label for="nama_pelanggan">Nama Pelanggan</label>
                  <input type="text" class="form-control" id="nama_pelanggan" autocomplete="off" required placeholder="Nama Pelanggan" autofocus>
              </div>
              <div class="form-group">
                  <label for="alamat_pelanggan">Alamat Pelanggan</label>
                  <textarea id="alamat_pelanggan" required class="form-control"></textarea>
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" style="background-color: #faa307;" class="btn text-white" id="check_out">Check Out</button>
          </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Transaksi -->
  <div class="modal fade" id="modal_selesai" role="dialog" aria-labelledby="exampleModalCenterTitle2" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered w-75" role="document">
      <div class="modal-content">
          <div class="modal-header" style="background-color: #faa307;">
              <h5 class="modal-title font-weight-bold text-white">Transaksi Berhasil!</h5>
              <button type="button" class="close close-menu-transaksi" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" class="mr-2 text-white">&times;</span>
              </button>
          </div>
          <div class="modal-body row">
            <div class="col-md-6">
                <a href="{{ url('Transaksi-umum/cetak') }}"  target="_blank">
                      <div class="card text-center shadow pulse c_nota" style="cursor: pointer; border-radius: 10px;">
                          <div class="card-body text-dark">
                              <h4 class="text-primary mb-2"><i class="fa fa-sticky-note fa-3x"></i></h4>
                              <h5 class="card-text">Cetak <br> Nota</h5>
                          </div>
                      </div>
                  </a>
              </div>
              <div class="col-md-6">
                  <div class="card text-center shadow pulse close-menu-transaksi" data-dismiss="modal" style="cursor: pointer; border-radius: 10px;">
                      <div class="card-body">
                          <h4 class="text-warning mb-2"><i class="fa fa-undo fa-3x"></i></h4>
                          <h5 class="card-text">Kembali <br> Transaksi</h5>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>  
<script>
    // untuk membuat kolom tunai dan diskon hanya menerima input angka beserta separator
    var tunai = document.getElementById('tunai');
    tunai.addEventListener('keyup', function(e){
        tunai.value = formatRupiah(this.value, '');
    });
    var nilai_diskon = document.getElementById('nilai_diskon');
    nilai_diskon.addEventListener('keyup', function(e){
        nilai_diskon.value = formatRupiah(this.value, '');
    });
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
        if(ribuan)
        {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
    $(function() {
        // Set Up header, token CSRF apalah
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // nav hide
        $('a[data-toggle="tab"]').on('hide.bs.tab', function(e) {
            var $old_tab = $($(e.target).attr("href"));
            var $new_tab = $($(e.relatedTarget).attr("href"));
            if ($new_tab.index() < $old_tab.index()) {
                $old_tab.css('position', 'relative').css("right", "0").show();
                $old_tab.animate({
                "right": "-100%"
                }, 300, function() {
                $old_tab.css("right", 0).removeAttr("style");
                });
                $('.search').val("");
                $('.menu-card').show();
            } else {
                $old_tab.css('position', 'relative').css("left", "0").show();
                $old_tab.animate({
                "left": "-100%"
                }, 300, function() {
                $old_tab.css("left", 0).removeAttr("style");
                });
                $('.search').val("");
                $('.menu-card').show();
            }
        });
        // nav show
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            var $new_tab = $($(e.target).attr("href"));
            var $old_tab = $($(e.relatedTarget).attr("href"));

            if ($new_tab.index() > $old_tab.index()) {
                $new_tab.css('position', 'relative').css("right", "-2500px");
                $new_tab.animate({
                "right": "0"
                }, 500);
                 $('.search').val("");
                 $('.menu-card').show();
            } else {
                $new_tab.css('position', 'relative').css("left", "-2500px");
                $new_tab.animate({
                "left": "0"
                }, 500);
                 $('.search').val("");
                 $('.menu-card').show();
            }
        });
        // Animasi perpindahan nav
        $(".pulse").click(function(){
            $(this).addClass("active").delay(300).queue(function(next){
                $(this).removeClass("active");
                next();
                $('.search').val("");
                $('.menu-card').show();
            });
        });
        // Live Search
        $('.search').keyup(function(event) {
            var filter = $(this).val();
            $('.nama-product').each(function() {
                var id = $(this).data('id');
                if($(this).text().search(new RegExp(filter, 'i')) < 0) {
                   $('#menu-'+id).hide();
                }
                else
                {
                    $('#menu-'+id).show();
                }
            });
        });
        // Set Button Transaksi agar Dihilangkan kalau kosong
        $('#btn_transaksi').attr('hidden', false);
        // untuk Tunai
        $('#tunai').keyup(function(event) {
            var nominal     = $('#total').text().replace("Rp. ", '');
            var subtotal    = nominal.split('.').join('');
            var tunai1      = $(this).val();
            var tunai       = tunai1.split('.').join('');
            var t_kembali   = tunai - subtotal;
            if (tunai > 0) {
                $('#kembali').text("Rp. "+separator(t_kembali));
            }
            else
            {
                $('#kembali').text("Rp. 0");
            }
        });
        // tunai jika nilai masih 0
        $('#tunai').click(function (e) { 
            e.preventDefault();
            if($(this).val() < 1)
            {
                $(this).select();
            }
        });
        // diskon jika nilai masih 0
        $('#nilai_diskon').click(function (e) { 
            e.preventDefault();
            if($(this).val() < 1)
            {
                $(this).select();
            }
        });
        // validasi qty
        $('#jumlah').on('keydown keyup', function(e){
            if ($(this).val() > $(this).data('stok') 
                && e.keyCode !== 46
                && e.keyCode !== 8
               ) {
               e.preventDefault();
               $(this).val($(this).data('stok'));
            }
        });
        // Untuk angka aja
        $(".angka").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
        // Set Data Table
        var table = $('#tabel_pesanan').DataTable({
            processing: true,
            order: true,
            paging:false,
            info:false,
            bFilter:false,
            language: {
                emptyTable: 'List Kosong'
            },
            columns: [
                {name: 'obatalkes_nama', visible:true},
                {name: 'obatalkes_kode', visible:true},
                {name: 'qty', visible:true},
                {name: 'diskon', visible:true},
                {name: 'aksi', visible:true, orderable: false, searchable: false}
            ]
        });
        // separator
        function separator(kalimat) {
            return kalimat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }
        // Add row
        $('.nm_product').click(function (e) { 
            e.preventDefault();
            var obatalkes_id = $(this).data('id');
            var id_obat = obatalkes_id;
            $.ajax({
                url     : "{{ url('transaksi-umum/add_row') }}",
                type    : "POST",
                data    : {obatalkes_id:obatalkes_id},
                dataType: "JSON",
                success : function (data) {
                    var counter = 1;
                    var obatalkes_id = data.obatalkes_id;
                    var obatalkes_nama = data.obatalkes_nama;
                    var obatalkes_kode = data.obatalkes_kode;
                    if($('#'+obatalkes_kode+'').text() == obatalkes_kode)
                    {
                        if(parseInt($('.jumlah').text()) >= $('.jumlah').data('stok')) 
                        {
                            swal({
                                title               : "Peringatan",
                                text                : 'Jumlah tidak boleh Melebihi Stok',
                                buttonsStyling      : false,
                                type                : 'warning',
                                showConfirmButton   : false,
                                timer               : 1000
                            });  
                            return false;
                        }
                        else
                        {
                            $('#jumlah'+obatalkes_id+'').text(parseFloat($('#jumlah'+obatalkes_id+'').text()) + 1);
                            var jumlah_obat = $('#'+obatalkes_kode+'_qty').val();
                            jumlah_obat = parseInt(jumlah_obat);
                            $('#'+obatalkes_kode+'_qty').val((jumlah_obat + 1));
                            var nominal     = $('#harga'+data.obatalkes_id+'').text().replace("Rp. ", '');
                            var harga       = parseFloat(nominal.split('.').join(''));
                            var jumlah      = parseFloat($('#jumlah'+obatalkes_id+'').text());
                            var subtotal    = harga*jumlah;
                            $('#total'+obatalkes_id+'').text('Rp. '+separator(subtotal));
                        }
                    }
                    else
                    {
                        table.row.add([
                            
                            "<div class='"+obatalkes_id+" obatalkes_nama'>"+obatalkes_nama+"<input type='hidden' name='id_obat[]' value='"+ obatalkes_id +"'</div>",
                            "<div class='"+obatalkes_id+" obatalkes_kode' id='"+ obatalkes_kode +"'>"+obatalkes_kode+"<input type='hidden' name='jumlah_obat[]' id='"+obatalkes_kode+"_qty' value='1'></div>",
                            "<label class='badge badge-danger jumlah' id='jumlah"+obatalkes_id+"' data-stok='"+data.stok+"'>"+counter+"</label>",
                            "<div id='diskon"+obatalkes_id+"' class='text-right diskon'>"+data.total_diskon+"</div>",
                            `<div><span style='cursor:pointer' class='text-primary ubah-list mr-3' data-toggle='tooltip' data-placement='top' 
                            title='Edit' product='`+obatalkes_nama+`' data-id='`+obatalkes_id+`'><i class='fa fa-pencil-alt'></i></span>
                            <span style='cursor:pointer' class='text-danger hapus-list' data-toggle='tooltip' data-placement='top' title='Hapus' 
                            data-id='`+obatalkes_id+`'><i class='fa fa-trash-alt'></i></span></div>`
                        ]).draw(false);
                    }
                    if(table.rows().count() < 1)
                    {
                        $('#diskon').text(data.diskon);
                        $('#total_diskon').text(data.total_diskon);
                        $('#total').text(data.tot_bayar);
                        $('#harga').val(data.tot_tr);
                        $('#obatalkes_id').val(data.obatalkes_id);
                        var tunai1      = $('#tunai').val();
                        var tunai       = tunai1.replace(".","");
                        var t_kembali   = tunai - data.tot_tr;
                        if (tunai > 0) {
                            $('#kembali').text("Rp. "+separator(t_kembali));
                        }
                    }
                    else
                    {
                        if(table.rows().count() < 2)
                        {
                            var nominal     = $('.subtotal').text().replace("Rp. ", '');
                            var subtotal    = nominal.split('.').join('');
                            $('#total_diskon').text(data.total_diskon);
                            $('#total').text('Rp. '+separator(subtotal,0,',','.'));
                            var tunai1      = $('#tunai').val();
                            var tunai       = tunai1.split('.').join('');
                            var t_kembali   = tunai - subtotal;
                            if (tunai > 0) {
                                $('#kembali').text("Rp. "+separator(t_kembali));
                            }
                        }
                        else
                        {
                            var subtotal = 0;
                            var diskon   = 0;
                            $('.subtotal').each(function(){
                                var nominal_harga   = $(this).text().replace("Rp. ", '');
                                var harga           = nominal_harga.split('.').join('');
                                subtotal            += parseInt(harga);
                            });
                            $('.diskon').each(function(){
                                var nominal_diskon  = $(this).text().replace("Rp. ", '');
                                var nilai_diskon    = nominal_diskon.split('.').join('');
                                diskon              += parseInt(nilai_diskon);
                            });
                            $('#total_diskon').text('Rp. '+separator(diskon));
                            $('#total').text('Rp. '+separator(subtotal));
                            var tunai1      = $('#tunai').val();
                            var tunai       = tunai1.split('.').join('');
                            var t_kembali   = tunai - subtotal;
                            if (tunai > 0) {
                                $('#kembali').text("Rp. "+separator(t_kembali));
                            }
                        }
                    }
                    $('#btn_transaksi').removeAttr('hidden');
                }
            })
        });
        // Buka Modul ubah pesanan
        $('#tabel_pesanan').on('click', '.ubah-list', function () {
            var id              = $(this).data('id');
            var product         = $(this).attr('product');
            var nominal_diskon  = $('#diskon'+id+'').text().replace("Rp. ", '');
            var diskon          = parseFloat(nominal_diskon.split('.').join(''));
            var jumlah          = parseFloat($('#jumlah'+id+'').text());
            var stok            = $('#jumlah'+id+'').data('stok');
            $('.judul').text("Product "+product);
            $('#id').val(id);
            $('#jumlah').val(jumlah);
            $('#jumlah').attr({
                'data-stok': stok
            });
            $('#nilai_diskon').val(diskon);
            $('#modal_ubah_list').modal('show');
        })
        // Update Pesanan
        $('#simpan_produk').on('click', function () {
            var form_list    = $('#form_list').serialize();
            id = $('#id').val();
            $('#jumlah'+id+'').text($('#jumlah').val());
            $('#diskon'+id+'').text('Rp. '+separator($('#nilai_diskon').val()));
            var nominal_harga       = $('#harga'+id+'').text().replace("Rp. ", '');
            var harga               = parseFloat(nominal_harga.split('.').join(''));
            var nominal_diskon      = $('#diskon'+id+'').text().replace("Rp. ", '');
            var diskon              = parseFloat(nominal_diskon.split('.').join(''));
            var jumlah              = parseFloat($('#jumlah'+id+'').text());
            var subtotal            = (harga*jumlah)-diskon;
            $('#total'+id+'').text('Rp. '+separator(subtotal));
            if(table.rows().count() < 2)
            {
                $('#total_diskon').text('Rp. '+separator($('#nilai_diskon').val()));
                $('#total').text('Rp. '+separator(subtotal,0,',','.'));
                $('#harga').val(subtotal);
                var tunai1      = $('#tunai').val();
                var tunai       = tunai1.split('.').join('');
                var t_kembali   = tunai - subtotal;
                if (tunai > 0) {
                    $('#kembali').text("Rp. "+separator(t_kembali));
                }
                $('#modal_ubah_list').modal('hide');
            }
            else
            {
                var nilai_subtotal = 0;
                var nilai_diskon   = 0;
                $('.subtotal').each(function(){
                    var nominal_harga   = $(this).text().replace("Rp. ", '');
                    var harga_plain     = nominal_harga.split('.').join('');
                    nilai_subtotal      += parseInt(harga_plain);
                });
                $('.diskon').each(function(){
                    var nominal_diskon  = $(this).text().replace("Rp. ", '');
                    var diskon_plain    = nominal_diskon.split('.').join('');
                    nilai_diskon        += parseInt(diskon_plain);
                });
                $('#total_diskon').text('Rp. '+separator(nilai_diskon));
                $('#total').text('Rp. '+separator(nilai_subtotal));
                var tunai1      = $('#tunai').val();
                var tunai       = tunai1.split('.').join('');
                var t_kembali   = tunai - nilai_subtotal;
                if (tunai > 0) {
                    $('#kembali').text("Rp. "+separator(t_kembali));
                }
                $('#modal_ubah_list').modal('hide');
            }
        })
         // Hapus Pesanan
         $('#tabel_pesanan').on('click', '.hapus-list', function () {
            table.row($(this).parents('tr')).remove().draw();
            if(table.rows().count() < 1) 
            {
                $('#btn_transaksi').attr('hidden', true);
                $('#diskon').text('Rp. 0');
                $('#total_diskon').text('Rp. 0');
                $('#total').text('Rp. 0');
                $('#harga').val('Rp. 0');
                $('#kembali').text("Rp. 0");
            }
            else
            {
                $('#btn_transaksi').removeAttr('hidden');
                if(table.rows().count() < 2)
                {
                    var nominal_subtotal    = $('.subtotal').text().replace("Rp. ", '');
                    var subtotal            = nominal_subtotal.split('.').join('');
                    var nominal_diskon      = $('.diskon').text().replace("Rp. ", '');
                    var diskon              = nominal_diskon.split('.').join('');
                    $('#total_diskon').text('Rp. '+separator(diskon,0,',','.'));
                    $('#total').text('Rp. '+separator(subtotal,0,',','.'));
                    var tunai1      = $('#tunai').val();
                    var obatalkes_id      = $('#obatalkes_id').val();
                    var tunai       = tunai1.split('.').join('');
                    var t_kembali   = tunai - subtotal;
                    if (tunai > 0) {
                        $('#kembali').text("Rp. "+separator(t_kembali));
                    }
                }
                else
                {
                    var nilai_subtotal = 0;
                    var nilai_diskon   = 0;
                    $('.subtotal').each(function(){
                        var nominal_harga   = $(this).text().replace("Rp. ", '');
                        var harga_plain     = nominal_harga.split('.').join('');
                        nilai_subtotal      += parseInt(harga_plain);
                    });
                    $('.diskon').each(function(){
                        var nominal_diskon  = $(this).text().replace("Rp. ", '');
                        var diskon_plain    = nominal_diskon.split('.').join('');
                        nilai_diskon        += parseInt(diskon_plain);
                    });
                    $('#total_diskon').text('Rp. '+separator(nilai_diskon));
                    $('#total').text('Rp. '+separator(nilai_subtotal));
                    var tunai1      = $('#tunai').val();
                    var tunai       = tunai1.split('.').join('');
                    var t_kembali   = tunai - nilai_subtotal;
                    if (tunai > 0) {
                        $('#kembali').text("Rp. "+separator(t_kembali));
                    }
                }
            }
        })
        // Tombol Transaksi pass ke Modal Data Pelanggan
        $('#btn_transaksi').on('click', function () {
            var nominal_tunai           = $('#tunai').val();
            var obatalkes_id           = $('#obatalkes_id').val();
            var tunai                   = nominal_tunai.split('.').join('');
            var nominal_kembalian       = $('#kembali').text().replace("Rp. ", '');
            var kembalian               = nominal_kembalian.split('.').join('');
            if(tunai == 0) {
                swal({
                    title               : "Peringatan",
                    text                : 'Nilai Tunai Harap Diisi!',
                    buttonsStyling      : false,
                    type                : 'warning',
                    showConfirmButton   : false,
                    timer               : 1000
                });  
                return false;
            } else if (kembalian < 0) {
                swal({
                    title               : "Peringatan",
                    text                : 'Nilai Tunai Kurang!',
                    buttonsStyling      : false,
                    type                : 'warning',
                    showConfirmButton   : false,
                    timer               : 1000
                });  
                return false;
            } else {
                $('#modal_info_pelanggan').modal('show');
            }
        })
        $('#check_out').click(function(event) {
            var nominal_total_harga     = $('#total').text().replace("Rp. ", '');
            var total_harga             = nominal_total_harga.split('.').join('');
            var nominal_tunai           = $('#tunai').val();
            var tunai                   = nominal_tunai.split('.').join('');
            var nominal_kembalian       = $('#kembali').text().replace("Rp. ", '');
            var kembali                 = nominal_kembalian.split('.').join('');
            var nominal_total_diskon    = $('#total_diskon').text().replace("Rp. ", '');
            var total_diskon            = nominal_total_diskon.split('.').join('');
            var nama_pelanggan          = $('#nama_pelanggan').val();
            var alamat_pelanggan        = $('#alamat_pelanggan').val();
            var obatalkes_id            = $('#obatalkes_id').val();
            var nama_produk            = [];
            $('.nama_produk').each(function() { 
                nama_produk.push($(this).text()); 
            });
            var kode_obat = [];
            $('.obatalkes_kode').each(function() {
                kode_obat.push($(this).text());
            });
            var jumlah                  = [];
            $('.jumlah').each(function() {
                jumlah.push($(this).text());
            });
            var discount                = [];
            $('.diskon').each(function() {
                discount.push($(this).text().replace("Rp. ", '').split('.').join(''));
            });
            var subtotal                = [];
            $('.subtotal').each(function() {
                subtotal.push($(this).text().replace("Rp. ", '').split('.').join(''));
            });
            if(nama_pelanggan == '') {
                swal({
                    title               : "Peringatan",
                    text                : 'Nama Pelanggan Harap Diisi!',
                    buttonsStyling      : false,
                    type                : 'warning',
                    showConfirmButton   : false,
                    timer               : 1000
                });  
                return false;
            } else if (alamat_pelanggan == '') {
                swal({
                    title               : "Peringatan",
                    text                : 'Alamat Pelanggan Harap Diisi!',
                    buttonsStyling      : false,
                    type                : 'warning',
                    showConfirmButton   : false,
                    timer               : 1000
                });  
                return false;
            } else {
                $.ajax({
                    url         : "{{ route('transaksi-umum.store') }}",
                    method      : "POST",
                    beforeSend  : function () {
                        swal({
                            title   : 'Menunggu',
                            html    : 'Memproses Data',
                            onOpen  : () => {
                                swal.showLoading();
                            }
                        })
                    },
                    data        : {obatalkes_id:obatalkes_id,total_harga:total_harga, total_diskon:total_diskon, nama_produk:nama_produk, 
                    jumlah:jumlah, diskon:discount, subtotal:subtotal, 
                    nama_pelanggan:nama_pelanggan, alamat_pelanggan:alamat_pelanggan,kode_obat:kode_obat,
                    tunai:tunai, kembali:kembali},
                    dataType    : "JSON",
                    success     : function (data) {
                        $('#subtotal').text("Rp. 0");
                        $('#diskon').text("Rp. 0");
                        $('#total_diskon').text("Rp. 0");
                        $('#total').text("Rp. 0");
                        $('#tunai').val(0);
                        $('#kembali').text("Rp. 0");
                        $('#btn_transaksi').attr('hidden', true);
                        swal.close()  
                        $('#modal_info_pelanggan').modal('hide');
                        $('#modal_selesai').modal('show');
                        table.clear().draw();              
                    },
                    error 		: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError);
                    }
                });
                return false;
            }
        });
        // close modal transaksi dan trigger reload
        $(".close-menu-transaksi").click(function(event) {
            location.reload();
        });
    });
</script>
@endsection