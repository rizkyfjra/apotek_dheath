@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Master Data</li>
            <li class="breadcrumb-item active">Master Stok Obat Alkes</li>
        </ol>

        <a class="btn btn-success btn-sm" href="javascript:void(0)" id="create"><i class="fas fa-wd fa-plus"></i> Tambah Stok Obat Alkes</a>
        
        <hr />
        <div class="table-responsive">
            <table id="main_table" class="table table-bordered table-hover w-100">
                <thead class="thead-light">
                    <tr>
                        <th scope="col" width="5%">No</th>
                        <th scope="col">Kode Obat Alkes</th>
                        <th scope="col">Produk Obat Alkes</th>
                        <th scope="col" width="12%">Stok</th>
                        <th scope="col" width="12%">Tanggal Masuk Barang</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modalHeader"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="form" name="form" class="form-horizontal">
            <div class="modal-body">
                
                <div class="form-group">
                    <label for="obatalkes_kode" class="control-label">Kode Obat Alkes</label>
                    <input type="text" class="form-control" id="obatalkes_kode" name="obatalkes_kode" placeholder="Kode Obat Alkes"
                        autocomplete="off">
                    <span id="obatalkes_kode-error" class="help-block text-danger p-1"></span>
                </div>
                <div class="form-group">
                    <label for="obatalkes_nama" class="control-label">Nama Produk Obat Alkes</label>
                    <input type="text" class="form-control" id="obatalkes_nama" name="obatalkes_nama" placeholder="Nama Obat Alkes"
                        autocomplete="off">
                    <span id="obataleks_nama-error" class="help-block text-danger p-1"></span>
                </div>
                <div class="form-group">
                    <label for="stok" class="control-label">Stok Obat Alkes</label>
                    <input type="text" class="form-control" id="stok" name="stok" placeholder="Stok obat Alkes"
                        autocomplete="off">
                    <span id="obataleks_nama-error" class="help-block text-danger p-1"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
            </div>
        </form>
        </div>
    </div>
</div>

<script>
    var lane;
    $(function() {
        // Set Up header, token CSRF
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Set Data Table
        var table = $('#main_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('stok.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'obatalkes_kode', name:'obatalkes_kode'},
                {data: 'obatalkes_nama', name:'obatalkes_nama'},
                {data: 'stok', name:'stok'},
                {data: 'created_date', name:'created_date'},
            ]
        });
        // Set Data Table Detail
        var table = $('.table_detail').DataTable({});
        // Trigger form create
        $('#create').click(function(){
            var lane = 'in';
            $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
            $('#id').val('');
            $('#form').trigger('reset');
            $('#modalHeader').text('Tambah Stok Obat Alkes');
            $('#modal').modal('show');
            $('.help-block').hide();
        });

        // Proses Created and update
        $('#saveBtn').click(function (e) {
            e.preventDefault();
            if(lane = 'in') {
                var url = "{{ route('stok.store') }}";
            }
            var stok            = $('#stok').val();
            var id_produk       = $('select[name=id_produk] option').filter(':selected').val();
            var expired_at      = $('#expired_at').val();
            $(this).html('<i class="fa fa-wd fa-clock-o"></i> Memproses..');
            $(this).attr('disabled',false);
            if(stok.length < 1 || id_produk == '' || expired_at == '') {
                if(stok.length < 1) {
                    validasi_stok();
                }
                if(id_produk == '') {
                    validasi_id_produk();
                }
                if(expired_at == '') {
                    validasi_expired_at();
                }
                    $(this).html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                    $(this).attr('disabled',false);
            }
            else
            { 
            $.ajax({
                data: $('#form').serialize(),
                url: url,
                type: "POST",
                beforeSend :function () {
                    swal({
                        title: 'Menunggu',
                        html: 'Memproses data',
                        onOpen: () => {
                        swal.showLoading()
                        }
                    })
                },
                dataType: 'json',
                success: function(data) {
                        swal({
                            title: "Berhasil!",
                            text: data.success,
                            type: "success"
                        });
                        $('#form').trigger('reset');
                        $(this).html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                        $(this).attr('disabled',false);
                        $('#modal').modal('hide');
                        table.draw();
                    }
                });
            }
        });
        // Trigger form return stok
        $('#return_stok').click(function(){
            var lane = 'out';
            $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
            $('#id').val('');
            $('#form').trigger('reset');
            $('#modalHeader').text('Return Stok Obat Alkes');
            $('#modal').modal('show');
            $('.help-block').hide();
        });
        
        // validasi stok
        function validasi_stok() {
            var panjang = $("#stok").val().length;
            var stok = $("stok").val();
            if(panjang < 1){
                $("#stok-error").html("Jumlah Stok harus Diisi");
                $("#stok-error").show().addClass("error");
                error_stok = true;
            }else{
                $("#stok-error").hide();
                $("#saveBtn").attr('disabled',false);
            }
        }
        
        // validasi tanggal kadaluarsa
        function validasi_expired_at() {
            var expired_at = $('#expired_at').val();
            if(expired_at == ''){
                $("#expired_at-error").html("Produk belum DIpilih");
                $("#expired_at-error").show().addClass("error");
                error_expired_at = true;
            }else{
                $("#expired_at-error").hide();
                $("#saveBtn").attr('disabled',false);
            }
        }
        // stok, angka saja
        $("#stok").keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#errmsg").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });
    });
</script>
@endsection
