<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>D'health</title>
    <!-- Custom fonts for this template-->
    <link href="{{ url('lte/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="{{ url('lte/vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ url('lte/css/sb-admin.css') }}" rel="stylesheet">
    <link href="{{ url('dropify/dist/css/dropify.css') }}" rel="stylesheet">
    <link href="{{ url('lightbox/dist/css/lightbox.css') }}" rel="stylesheet">
    <script src="{{url('lte/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{url('dropify/dist/js/dropify.js') }}"></script>
    <script src="{{url('lightbox/dist/js/lightbox.js') }}"></script>
    <script src="{{url('lte/vendor/datatables/jquery.datatables.js') }}"></script>
    <script src="{{url('lte/vendor/datatables/datatables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="http://services.iperfect.net/js/IP_generalLib.js"></script>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
    @if ( Request::segment(1) === 'transaksi-umum')
    <style>
        #tabel-bawah,
        thead,
        tbody,
        tr,
        td {
            border-top: none !important;
        }
        .input {
            border-radius: 10px;
            border-color: #f2a654;
        }
        .input2 {
            border-radius: 10px;
            border-color: #f2a654;
        }
        @keyframes bounce {
            0% {
                transform: scale(1);
            }
            50% {
                transform: scale(1.1);
            }
            100% {
                transform: scale(1);
            }
        }
        .pulse.active {
            animation: bounce 0.3s ease-out 1;
        }
        .controlgroup-textinput {
            padding-top: .22em;
            padding-bottom: .22em;
        }
        .nav-tabs .nav-link:not(.active) {
            border-color: #f2a654 !important;
            color: grey;
        }
        .nav-tabs .nav-link.active {
            border-color: #f2a654 !important;
            background-color: #faa307 !important;
            font-weight: bold !important;
            color: white !important;
        }
        .nav-tabs .nav-link.active h3 {
            color: white !important;
        }
    </style>
    @endif
    <style>
        body {
            padding-right: 0px !important;
        }
    </style>

</head>

<body id="page-top">

    <nav class="navbar navbar-expand navbar-light" style="background-color: #1a95e7;" static-top">
        <a class="navbar-brand mr-1 text-white" href="/admin/dashboard"> E-Prescription  </a>

        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>
        <!-- Navbar Search -->
        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            <div class="input-group">
                <div class="input-group-append">
                </div>
            </div>
        </form>

        <!-- Navbar -->
        <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item dropdown no-arrow mx-1">
                <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <?php
                        date_default_timezone_set("Asia/Jakarta");
                    ?>
                    <?= date("d M Y H:i:s"); ?>
                </a>
            </li>
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user-circle fa-fw"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">{{ ucfirst(Auth::user()->name) }}</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
                </div>
            </li>
        </ul>

    </nav>

    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="sidebar navbar-nav navbar-dark bg-dark">
            <li class="nav-item">
                <a class="nav-link" href="/admin/dashboard">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/transaksi-umum">
                    <i class="fas fa-fw fa-cart-plus"></i>
                    <span>Transaksi</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/resep">
                    <i class="fas fa-fw fa-cart-plus"></i>
                    <span>Resep Pelanggan</span>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-archive"></i>
                    <span>Master Data</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                    <h6 class="dropdown-header">Master Data :</h6>
                    <a class="dropdown-item" href="/stok"><i class="fas fa-fw fa-medkit"></i> Stok Obat & Alkes</a>
                    <a class="dropdown-item" href="/signa"><i class="fas fa-fw fa-user-circle"></i> Stok Signa</a>
                    <!-- <a class="dropdown-item" href="/jenis-obat"><i class="fas fa-fw fa-list-ul"></i> Jenis Obat</a>
                    <a class="dropdown-item" href="/kategori-obat"><i class="fas fa-fw fa-filter"></i> Kategori Obat</a>
                    <a class="dropdown-item" href="/produk"><i class="fas fa-fw fa-truck"></i> Produk</a>
                    <a class="dropdown-item" href="/dokter"><i class="fas fa-fw fa-user-md"></i> Dokter</a>
                    <a class="dropdown-item" href="/vendor"><i class="fas fa-fw fa-truck"></i> Vendor</a>
                    <a class="dropdown-item" href="/klinik"><i class="fas fa-fw fa-hospital"></i> Klinik</a>
                </div> -->
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link" href="/laba-rugi">
                    <i class="fas fa-fw fa-balance-scale"></i>
                    <span>Hitung Laba Rugi</span></a>
            </li> -->
        </ul>

        <div id="content-wrapper">
            <div class="container-fluid">

                @yield('content')

                <footer class="sticky-footer">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            {{-- <span>E-Prescription / Resep Obat Digital - Rizky Fajar Anugrah - {{ date('Y') }}</span> --}}
                            <span>TEST WEB DEVELOPER <b>d'health</b> {{ date('Y') }}</span>
                        </div>
                    </div>
                </footer>

            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /#wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Yakin keluar?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Klik "logout" jika ingin keluar.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-warning" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>

  <!-- src="{{url('adminlte/dist/img/user2-160x160.jpg')}}" -->
  <!-- Bootstrap core JavaScript-->
  <script src="{{url('lte/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{url('lte/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{url('lte/js/sb-admin.min.js') }}"></script>

</body>

</html>
