@extends('layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/admin/dashboard">Dashboard</a>
            </li>
            <li class="breadcrumb-item">Master Data</li>
            <li class="breadcrumb-item active">Kategori Obat</li>
        </ol>

        <!-- Page Content -->
        <!-- Icon Cards-->
        <a class="btn btn-success btn-sm" href="javascript:void(0)" id="create"><i class="fas fa-wd fa-plus"></i> Tambah Data Kategori</a>
        <hr />
        <div class="table-responsive">
            <table class="table table-bordered table-hover w-100">
                <thead class="thead-light">
                    <tr>
                        <th scope="col" width="5%">No</th>
                        <th scope="col">Kategori</th>
                        <th scope="col" width="12%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modalHeader"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="form" name="form" class="form-horizontal">
            <div class="modal-body">
                <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="kategori" class="control-label">Nama Kategori</label>
                        <input type="text" class="form-control" id="kategori" name="kategori" placeholder="Nama Kategori" autocomplete="off">
                        <span id="kategori-error" class="help-block text-danger p-1"></span>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
            </div>
        </form>
        </div>
    </div>
</div>
<script>
    $(function() {
        // Set Up header, token CSRF apalah
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Set Data Table
        var table = $('.table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('kategori-obat.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'kategori', name:'kategori'},
                {data: 'action', name:'action', orderable: false, searchable: false}
            ]
        });
        // Trigger form create
        $('#create').click(function(){
            $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
            $('#id').val('');
            $('#form').trigger('reset');
            $('#modalHeader').text('Tambah Data Kategori');
            $('#modal').modal('show');
            $('.help-block').empty();
        });
        // Trigger form edit
        $('body').on('click', '.editData', function () {
            var id = $(this).data('id');
            $.get("{{ route('kategori-obat.index') }}"+'/'+id+'/edit', function(data) {
                $('#saveBtn').html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                $('#id').val(data.id);
                $('#modalHeader').text('Sunting Data '+data.kategori);
                $('#modal').modal('show');
                $('.help-block').empty();
                $('#kategori').val(data.kategori);
            })
        });
        // Proses Validasi Create dan Update
        $('#saveBtn').click(function (e) { 
            e.preventDefault();
            $(this).html('<i class="fa fa-wd fa-clock-o"></i> Memproses..');
            $(this).attr('disabled',true);
            validasi_kategori();
        });
        // proses create dan update
        function proses() {
            $.ajax({
                data: $('#form').serialize(),
                url: "{{ route('kategori-obat.store') }}",
                type: "POST",
                beforeSend :function () {
                    swal({
                        title: 'Menunggu',
                        html: 'Memproses data',
                        onOpen: () => {
                        swal.showLoading()
                        }
                    })      
                },
                dataType: 'json',
                success: function(data) {
                    swal({
                        title: "Berhasil!",
                        text: data.success,
                        type: "success"
                    });
                    $('#form').trigger('reset');
                    $("#saveBtn").html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                    $("#saveBtn").attr('disabled',false);
                    $('#modal').modal('hide');
                    table.draw();
                }
            });
        }
        // hapus data
        $('body').on('click', '.deleteData', function () {
            var id = $(this).data('id');
            swal({
                title: "Apa Anda yakin untuk Menghapus Data ini?",
                text: "Data tidak dapat Dikembalikan setelah Terhapus",
                type: "error",
                showCancelButton: true,
                cancelButtonClass: '#DD6B55',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Hapus',
            }).then(function (e) {
                if(e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'DELETE',
                        url: "{{ url('kategori-obat') }}"+'/'+id,
                        data: {_token: CSRF_TOKEN},
                        beforeSend :function () {
                            swal({
                                title: 'Menunggu',
                                html: 'Memproses Data',
                                onOpen: () => {
                                swal.showLoading()
                                }
                            })      
                        }, 
                        dataType: 'JSON',
                        success: function(data) {
                            swal(
                                'Berhasil',
                                'Data Berhasil Dihapus!',
                                'success'
                            );
                            table.draw();
                        }
                    })
                }
                else
                {
                    swal(
                        'Batal',
                        'Anda membatalkan penghapusan',
                        'error'
                    );
                }
            })
        })
        // validasi Kategori
        function validasi_kategori() {
            var panjang     = $("#kategori").val().length;
            var kategori    = $('#kategori').val();
            var id          = $('#id').val();
            if(panjang < 1){
                $("#kategori-error").html("Kategori harus Diisi");
                $("#kategori-error").show().addClass("error");
                error_kategori = true;
                $("#saveBtn").html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                $("#saveBtn").attr('disabled',false);
            }
            else
            {
                $.ajax({
                    url: "{{ url('get-kategori') }}",
                    data: {kategori:kategori, id:id},
                    beforeSend :function () {
                            swal({
                                title: 'Menunggu',
                                html: 'Memvalidasi Data',
                                onOpen: () => {
                                swal.showLoading()
                                }
                            })      
                        }, 
                    dataType: 'JSON',
                    success: function(data) {
                        if(data.length > 0) {
                            swal.close();
                            $("#kategori-error").html("Kategori "+kategori+" sudah Digunakan");
                            $("#kategori-error").show().addClass("error");
                            error_kategori = true;
                            $("#saveBtn").html('<i class="fa fa-wd fa-paper-plane"></i> Submit');
                            $("#saveBtn").attr('disabled',false);
                        }
                        else
                        {
                            $("#kategori-error").hide();
                            proses();
                        }
                    }
                })
            }
        }
    });
</script>
@endsection