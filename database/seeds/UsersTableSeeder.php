<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => Str::random(20),
            'email'     => Str::random(10).'@mail.com',
            'username'  => 'admin',
            'password'  => bcrypt('admin')
        ]);
    }
}
