<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukTableM extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_m', function (Blueprint $table) {
            $table->id();
            $table->string('nama_produk');
            $table->integer('id_jenis_obat');
            $table->integer('id_kategori');
            $table->integer('stok');
            $table->text('foto_obat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_m');
    }
}
