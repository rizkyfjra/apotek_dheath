<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('vendor_m');
        Schema::create('mst_vendor', function (Blueprint $table) {
            $table->bigIncrements('id_vendor');
            $table->string('kode_vendor');
            $table->string('nama_vendor');
            $table->string('nama_lainnya');
            $table->text('alamat_vendor');
            $table->string('kodepos_vendor');
            $table->string('propinsi_vendor');
            $table->string('kabupaten_vendor');
            $table->string('telp_vendor');
            $table->string('fax_vendor');
            $table->string('npwp_vendor');
            $table->string('website_vendor');
            $table->string('jenis_vendor');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_m');
    }
}
