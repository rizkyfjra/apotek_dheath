<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInfoPelangganForTrnTransaksiUmum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi_umum_t', function (Blueprint $table) {
            $table->string('nama_pelanggan')->after('kembali');
            $table->text('alamat_pelanggan')->after('nama_pelanggan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi_umum_t', function (Blueprint $table) {
            //
        });
    }
}
