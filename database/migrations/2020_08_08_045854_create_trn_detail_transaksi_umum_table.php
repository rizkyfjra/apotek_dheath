<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Stripe\Discount;

class CreateTrnDetailTransaksiUmumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi_umum_t', function (Blueprint $table) {
            $table->id();
            $table->integer('id_transaksi');
            $table->integer('id_produk');
            $table->integer('jumlah');
            $table->string('discount');
            $table->string('subtotal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksi_umum_t');
    }
}
