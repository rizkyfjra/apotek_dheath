<?php

use App\Http\Controllers\KlinikController as ControllersKlinikController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\KlinikController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/login', 'AuthController@index');

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/admin', 'AdminController@index');
Route::get('/admin/dashboard', 'AdminController@dashboard');
Route::get('login', 'AuthController@index');
Route::post('post-login', 'AuthController@postLogin');
Route::get('registration', 'AuthController@registration');
Route::post('post-registration', 'AuthController@postRegistration');
Route::get('logout', 'AuthController@logout');
// Modul Stok Obat ALkes
Route::resource('/stok', 'StokMController');
Route::get('get-kategori', 'StokMController@get_data');
Route::resource('/stokobat', 'StokobatalkesController');
Route::get('/stokobat/tambahstok', 'StokobatalkesController@tambahstok');
Route::post('/stokobat/tambahstok', 'StokobatalkesController@tambahstok');
Route::get('/transaksi/index', 'TransaksiController@index');
// Modul Vendor
Route::resource('/vendor', 'VendorController');
// Modul Dokter
Route::resource('/resep', 'ResepMController');
// Modul Signa
Route::resource('/signa', 'SignaController');
// Modul Klinik
Route::resource('/klinik', 'KlinikController');
// Modul Master Produk
Route::resource('/produk', 'ProdukController');
// Modul Master Jenis
Route::resource('/jenis-obat', 'JenisObatController');
// Modul Transaksi Umum
Route::resource('/transaksi-umum', 'TransaksiUmumController');
Route::post('/transaksi-umum/add_row', 'TransaksiUmumController@add_row');
Route::resource('/transaksi', 'Transaksi');
// Modul Master Kategori
Route::resource('/kategori-obat', 'KategoriObatController');
Route::get('get-kategori', 'KategoriObatController@get_data');
// Modul Laba Rugi
Route::get('/laba-rugi', function () {
    return view('laba_rugi/index');
});

Auth::routes();

Route::get('/admin/dashboard', 'AdminController@dashboard')->name('home');
