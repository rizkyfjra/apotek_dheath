<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiUmum extends Model
{
    protected $table = 'transaksi_umum_t';
    protected $fillable = ['kode_transaksi','nama_pelanggan','alamat_pelanggan', 'total_harga', 'total_discount', 'tunai', 'kembali'];
}
