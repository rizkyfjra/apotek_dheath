<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signa extends Model
{
    protected $table = 'signa_m';
    protected $fillable = ['signa_id','signa_kode','signa_nama','created_date','is_active'];
}
