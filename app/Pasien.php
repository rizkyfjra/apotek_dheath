<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'pasien_m';
    protected $fillable = ['pasien_id', 'transaksi_id', 'nama_pelanggan', 'alamat_pelanggan'];
}
