<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $table = 'dokter_m';
    protected $fillable = ['nama_pegawai', 'tanggal_lahir', 'tempat_lahir', 'jenis_kelamin', 'agama', ];
}
