<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk_m';
    protected $fillable = ['nama_produk', 'id_jenis_obat', 'id_kategori', 'harga', 'foto_obat'];
}
