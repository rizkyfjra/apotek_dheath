<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaksiUmum extends Model
{
    protected $table = 'detail_transaksi_umum_t';
    protected $fillable = ['id_transaksi', 'id_produk', 'jumlah', 'discount', 'subtotal'];
}
