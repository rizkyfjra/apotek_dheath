<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisObat extends Model
{
    protected $table = 'jenis_obat_m';
    protected $fillable = ['id','jenis_obat','created_at','update_at'];
}
