<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StokT extends Model
{
    protected $table = 'stok_t';
    protected $fillable = ['id', 'id_stok', 'stok_masuk','stok_keluar','stok_retur','expired_at', 'created_at', 'update_at'];
}
