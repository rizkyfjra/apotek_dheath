<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'vendor_m';
    protected $fillable = ['id_vendor', 'kode_vendor', 'nama_vendor', 'nama_lainnya', 'alamat_vendor', 'kodepos_vendor', 'propinsi_vendor', 'kabupaten_vendor','jenis_vendor', 'telp_vendor', 'fax_vendor', 'npwp_vendor', 'email','website_vendor'];
}
