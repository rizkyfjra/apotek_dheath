<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObatAlkesM extends Model
{
    protected $table = 'obatalkes_m';
    protected $primaryKey = 'obatalkes_id';
    public $timestamps = false;
}
