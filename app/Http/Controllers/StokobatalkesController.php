<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\StokobatalkesM;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class StokobatalkesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = StokobatalkesM::all()->sortBy('stokobatalkes_id');
            return datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('tanggal_kadaluarsa', function ($data)
                    {
                        return date('d-m-Y', strtotime($data->tanggal_kadaluarsa));
                    })
                    ->addColumn('action', function($row) {
                        $btn = '<a href="javascript:void(0)" data-stokobatalkes_id="'.$row->stokobatalkes_id.'" 
                        class="btn btn-primary btn-sm editData">Edit</a>
                        <a href="javascript:void(0)" data-stokobatalkes_id="'.$row->stokobatalkes_id.'" 
                        class="btn btn-danger btn-sm deleteData">Delete</a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('stokobat.index');
    }
   
   
    public function store(Request $request)
    {
        
        DB::table('stokobatalkes_m')->updateOrCreate([
        'stokobatalkes_id' => $request->stokobatalkes_id
        ],
            [
                'jeniskelompok'         => $request->jeniskelompok,
                'jenis_obatalkes'       => $request->jenis_obatalkes,
                'kode_obat'             => $request->kode_obat,
                'nama_stokobatalkes'    => $request->nama_stokobatalkes,
                'kategori'              => $request->kategori,
                'tanggal_kadaluarsa'    => $request->tanggal_kadaluarsa,
                'stok_masuk'            => $request->stok_masuk,
                'stok_keluar'           => $request->stok_keluar,
                'stok_akhir'            => $request->stok_akhir,
            ]
        );
        return response()->json([
            'success'   => 'Penambahan Data Stok Obat Alkes Berhasil!'
        ]);
    }

    public function edit($stokobatalkes_id)
    {
        $model = StokobatalkesM::find($stokobatalkes_id);
        return response()->json($model);
    }

    public function destroy($stokobatalkes_id)
    {
        StokobatalkesM::destroy($stokobatalkes_id);
        return response()->json([
            'sucess'    => 'Data Berhasil Dihapus!'
        ]);
    }

    // public function index()
    // {
    //     $model = new StokobatalkesM();


    // 	return view('stokobat.index',['model'=>$model]);
    // }
    // public function tambahstok(Request $request)
    // {
    //     if($request->ajax()) {
    //         $data = StokobatalkesM::all()->sortBy('stokobatalkes_id');
    //         return datatables::of($data)
    //                 ->addIndexColumn()
    //                 ->editColumn('tanggal_kadaluarsa', function ($data)
    //                 {
    //                     return date('d-m-Y', strtotime($data->tanggal_kadaluarsa));
    //                 })
    //                 ->make(true);
    //     }
    // 	return view('stokobat.tambahstok');
    // }
    // public function retur()
    // {
    //     $title = 'Retur';
    //     $content = 'Retur Obat';

    //     return view('stokobat/retur', compact('title','content'));
    // }
}

?>