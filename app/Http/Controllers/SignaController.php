<?php

namespace App\Http\Controllers;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use App\JenisObat;
use App\Signa;
use App\Pasien;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SignaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data =  DB::table('signa_m')->orderBy('signa_nama')->get();
            return datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_date', function ($data)
                    {
                        return date('d M Y', strtotime($data->created_date));
                    })
                    ->addColumn('action', function($row) {
                        $btn = '<a href="javascript:void(0)" data-signa_id="'.$row->signa_id.'"
                        class="btn btn-danger btn-sm deleteData">Delete</a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('signa.index');
    }

    public function store(Request $request)
    {
        Signa::Create([
        
            'signa_nama' => $request->signa_nama,
            'signa_kode' => $request->signa_kode,
            'is_active'  => 1,
            ]
        );
        return response()->json([
            'success'   => 'Transaksi Data Berhasil!'
        ]);
    }

    public function edit($id)
    {
        $jenisobat = Signa::find($id);
        return response()->json($jenisobat);
    }

    public function destroy($id)
    {
        Signa::destroy($id);
        return response()->json([
            'sucess'    => 'Data Berhasil Dihapus!'
        ]);
    }
}
