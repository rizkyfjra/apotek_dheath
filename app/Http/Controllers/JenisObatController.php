<?php

namespace App\Http\Controllers;
use Yajra\DataTables\DataTables;
use App\JenisObat;
use Illuminate\Http\Request;

class JenisObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = JenisObat::all()->sortBy('jenis_obat');
            return datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row) {
                        $btn = '<a href="javascript:void(0)" data-id="'.$row->id.'" 
                        class="btn btn-primary btn-sm editData">Edit</a>
                        <a href="javascript:void(0)" data-id="'.$row->id.'" 
                        class="btn btn-danger btn-sm deleteData">Delete</a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('jenisobat.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        JenisObat::updateOrCreate([
                'id'        => $request->id
            ],
            [
                'jenis_obat'    => $request->jenis_obat,
            ]
        );
        return response()->json([
            'success'   => 'Transaksi Data Berhasil!'
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenisobat = JenisObat::find($id);
        return response()->json($jenisobat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JenisObat  $jenisObat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        JenisObat::destroy($id);
        return response()->json([
            'sucess'    => 'Data Berhasil Dihapus!'
        ]);
    }
}
