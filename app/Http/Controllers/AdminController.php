<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class AdminController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$title = 'Ini halaman home';
    	$content = 'Saat ini kita berada di halaman Home onphpid.com';

    	return view('admin/admin', compact('title', 'content'));
    }

    public function dashboard()
    {
    	$title = 'Ini halaman HTML';
    	$content = 'Saat ini kita berada di halaman HTML onphpid.com';

    	return view('admin/dashboard', compact('title', 'content'));
    }
}

?>