<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\TransaksiStok;
use App\StokM;

class Transaksi extends BaseController
{
    public function index()
    {
        $data = [
            'produk'    => DB::table('produk_m')
                        ->join('stok_m', 'stok_m.id_produk', '=', 'produk_m.id')
                        ->where('stok_m.stok', '>', 0)
                        ->get(),
            'info'      => DB::table('produk_m')
                        ->join('stok_m', 'stok_m.id_produk', '=', 'produk_m.id')
                        ->where('produk_m.id', '=', '1')
                        ->get()
        ];
        return view('transaksi/transaksi_list', $data);
    }

    // public function store(Request $request)
    // {
    //     $xyz                = [];
    //     $stok_awal          = DB::table('stok_m')
    //                         ->where('id_produk', $request->id_produk)
    //                         ->pluck('stok');
    //     $stok_per_expire    = DB::table('stok_t')
    //                         ->join('stok_m', 'stok_m.id', '=', 'stok_t.id_stok')
    //                         ->where('stok_m.id_produk', $request->id_produk)
    //                         ->orderBy('stok_t.expired_at')
    //                         ->groupBy('stok_t.expired_at')
    //                         ->pluck('stok_t.expired_at');
    //     for($i = 0; $i < count($stok_per_expire); $i++)
    //     {
    //         $stok       = 0;
    //         $get_stok   = DB::table('stok_t')
    //                         ->where('expired_at', $stok_per_expire[$i])
    //                         ->get();
    //         $id_stok    = $get_stok[0]->id_stok;
    //         for($j = 0; $j < count($get_stok); $j++)
    //         {
    //             if(count($get_stok) > 1)
    //             {
    //                 $stok   += ($get_stok[$j]->stok_masuk - ($get_stok[$j]->stok_keluar + $get_stok[$j]->stok_retur));
    //             }
    //             else
    //             {
    //                 $stok   = ($get_stok[0]->stok_masuk - ($get_stok[0]->stok_keluar + $get_stok[0]->stok_retur));
    //             }
    //         }
    //         if($stok > 0)
    //         {
    //             $xyz[]  = [
    //                 'stok'          => $stok,
    //                 'expired_at'    => $stok_per_expire[$i]
    //             ];
    //         }
    //     }
    //     if($request->stok_keluar == $stok_awal[0])
    //     {
    //         for($k = 0; $k < count($xyz); $k++)
    //         {
    //             TransaksiStok::create([
    //                 'id_stok'       => $id_stok,
    //                 'stok_masuk'    => 0,
    //                 'stok_keluar'   => $xyz[$k]['stok'],
    //                 'stok_retur'    => 0,
    //                 'expired_at'    => $xyz[$k]['expired_at']
    //             ]);
    //         }
    //         StokM::where('id_produk', $request->id_produk)
    //             ->update([
    //                 'stok'      => 0
    //             ]);
    //         return redirect('/transaksi')->with('status', 'Yatta!');
    //     }
    //     elseif($xyz[0]['stok'] >= $request->stok_keluar)
    //     {
    //         TransaksiStok::create([
    //             'id_stok'       => $id_stok,
    //             'stok_masuk'    => 0,
    //             'stok_keluar'   => $request->stok_keluar,
    //             'stok_retur'    => 0,
    //             'expired_at'    => $stok_per_expire[0]
    //         ]);
    //         StokM::where('id_produk', $request->id_produk)
    //             ->update([
    //                 'stok'      => ($stok_awal[0] - $request->stok_keluar)
    //             ]);
    //         return redirect('/transaksi')->with('status', 'Yatta!');
    //     }
    //     else
    //     {
    //         TransaksiStok::create([
    //             'id_stok'       => $id_stok,
    //             'stok_masuk'    => 0,
    //             'stok_keluar'   => $xyz[0]['stok'],
    //             'stok_retur'    => 0,
    //             'expired_at'    => $stok_per_expire[0]
    //         ]);
    //         TransaksiStok::create([
    //             'id_stok'       => $id_stok,
    //             'stok_masuk'    => 0,
    //             'stok_keluar'   => ($request->stok_keluar - $xyz[0]['stok']),
    //             'stok_retur'    => 0,
    //             'expired_at'    => $stok_per_expire[1]
    //         ]);
    //         StokM::where('id_produk', $request->id_produk)
    //             ->update([
    //                 'stok'      => ($stok_awal[0] - $request->stok_keluar)
    //             ]);
    //         return redirect('/transaksi')->with('status', 'Yatta!');
    //     }
    // }

    public function store(Request $request)
    {
        $xyz                = [];
        $stok_awal          = DB::table('stok_m')
                            ->where('id_produk', $request->id_produk)
                            ->pluck('stok');
        $stok_per_expire    = DB::table('stok_t')
                            ->join('stok_m', 'stok_m.id', '=', 'stok_t.id_stok')
                            ->where('stok_m.id_produk', $request->id_produk)
                            ->orderBy('stok_t.expired_at')
                            ->groupBy('stok_t.expired_at')
                            ->pluck('stok_t.expired_at');
        for($i = 0; $i < count($stok_per_expire); $i++)
        {
            $stok       = 0;
            $get_stok   = DB::table('stok_t')
                            ->where('expired_at', $stok_per_expire[$i])
                            ->get();
            $id_stok    = $get_stok[0]->id_stok;
            for($j = 0; $j < count($get_stok); $j++)
            {
                if(count($get_stok) > 1)
                {
                    $stok   += ($get_stok[$j]->stok_masuk - ($get_stok[$j]->stok_keluar + $get_stok[$j]->stok_retur));
                }
                else
                {
                    $stok   = ($get_stok[0]->stok_masuk - ($get_stok[0]->stok_keluar + $get_stok[0]->stok_retur));
                }
            }
            if($stok > 0)
            {
                $xyz  = [
                    'stok'          => $stok,
                    'expired_at'    => $stok_per_expire[$i]
                ];
            }
        }
        if($request->stok_keluar == $stok_awal)
        {
            for($k = 0; $k < count($xyz); $k++)
            {
                TransaksiStok::create([
                    'id_stok'       => $id_stok,
                    'stok_masuk'    => 0,
                    'stok_keluar'   => $xyz[$k]['stok'],
                    'stok_retur'    => 0,
                    'expired_at'    => $xyz[$k]['expired_at']
                ]);
            }
            StokM::where('id_produk', $request->id_produk)
                ->update([
                    'stok'      => 0
                ]);
            return redirect('/transaksi')->with('status', 'Yatta!');
        }
        elseif($xyz >= $request->stok_keluar)
        {
            TransaksiStok::create([
                'id_stok'       => $id_stok,
                'stok_masuk'    => 0,
                'stok_keluar'   => $request->stok_keluar,
                'stok_retur'    => 0,
                'expired_at'    => $stok_per_expire[0]
            ]);
            StokM::where('id_produk', $request->id_produk)
                ->update([
                    'stok'      => ($stok_awal[0] - $request->stok_keluar)
                ]);
            return redirect('/transaksi')->with('status', 'Yatta!');
        }
        else
        {
            TransaksiStok::create([
                'id_stok'       => $id_stok,
                'stok_masuk'    => 0,
                'stok_keluar'   => $xyz[0]['stok'],
                'stok_retur'    => 0,
                'expired_at'    => $stok_per_expire[0]
            ]);
            TransaksiStok::create([
                'id_stok'       => $id_stok,
                'stok_masuk'    => 0,
                'stok_keluar'   => ($request->stok_keluar - $xyz[0]['stok']),
                'stok_retur'    => 0,
                'expired_at'    => $stok_per_expire[1]
            ]);
            StokM::where('id_produk', $request->id_produk)
                ->update([
                    'stok'      => ($stok_awal[0] - $request->stok_keluar)
                ]);
            return redirect('/transaksi')->with('status', 'Yatta!');
        }
    }
}
