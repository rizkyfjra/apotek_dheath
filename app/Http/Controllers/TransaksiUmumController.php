<?php

namespace App\Http\Controllers;

use App\TransaksiUmum;
use App\Produk;
use Illuminate\Http\Request;
use App\StokM;
use App\Pasien;
use Illuminate\Support\Facades\DB;
use App\DetailTransaksiUmum;
use App\ObatAlkesM;

class TransaksiUmumController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = [
            'produk'    => Produk::all(),
            'kategori'  => DB::table('kategori_m')
                        ->distinct()
                        ->join('obatalkes_m', 'kategori_m.id', '=', 'obatalkes_m.id_kategori')
                        ->select('kategori_m.*')
                        ->get()
        ];
        return view('transaksi_umum.index', $data);
    }

    public function add_row(Request $request)
    {
        $obatalkes_id      = $request->obatalkes_id;
        $produk         = DB::table('obatalkes_m')
                        ->where('obatalkes_id', '=', $obatalkes_id)
                        ->get();
        $stok           = DB::table('obatalkes_m')
                        ->where('obatalkes_id', '=', $obatalkes_id)
                        ->get();
        $obatalkes_nama    = $produk[0]->obatalkes_nama;
        $obatalkes_kode    = $produk[0]->obatalkes_kode;
        // $harga          = $produk[0]->harga;
        // $total          = "Rp. ".number_format($harga,0,'.','.');
        $stok           = $stok[0]->stok;
        $diskon         = 0;
        $total_diskon   = 'Rp. 0';
        return response()->json([
            'status'        => 'Sukses',
            'obatalkes_id'     => $obatalkes_id,
            'obatalkes_nama'   => $obatalkes_nama,
            'obatalkes_kode'   => $obatalkes_kode,
            // 'total' 		=> $total,
			'diskon'		=> $diskon,
			'total_diskon'	=> $total_diskon,
			// 'tot_bayar'		=> $total,
			// 'tot_tr'		=> $harga,
			'stok'			=> $stok
        ]);
    }

    public function store(Request $request)
    {
        $stok_awal          = DB::table('obatalkes_M')
                            ->where('obatalkes_id', $request->obatalkes_id)
                            ->pluck('stok');
        
        
        
        $total_harga        = 0;
        $nama_produk        = $request->nama_produk;
        $jumlah             = $request->jumlah;
        $diskon             = 0;
        $total_harga        = 0;
        $tanggal            = date('Y-m-d H:i:s');
        $default_tanggal    = date('dmy');
        $jumlah_transaksi   = DB::table('transaksi_umum_t')
                            ->where('created_at', $tanggal)
                            ->count();
        if($jumlah_transaksi < 1)
        {
            $trn = TransaksiUmum::create([
                    'kode_transaksi'    => rand(),
                    'total_harga'       => $total_harga,
                    'total_discount'    => 0,
                    'tunai'             => $request->tunai,
                    'kembali'           => 0,
                    'nama_pelanggan'    => $request->nama_pelanggan,
                    'alamat_pelanggan'       => $request->alamat_pelanggan,
                ]);
            $usr = Pasien::create([
                'transaksi_id'      => $trn->id,
                'nama_pelanggan'    => $request->nama_pelanggan,
                'alamat_pelanggan'       => $request->alamat_pelanggan,
            ]);

            $jumlah_jenis_obat = count($request->kode_obat);
            if ($jumlah_jenis_obat > 0){
                $i = 0;
                foreach ($request->kode_obat as $ko){
                    $obat = ObatAlkesM::where('obatalkes_kode',$ko)->first();
                    $detail_transaksi = new DetailTransaksiUmum();
                    $detail_transaksi->id_transaksi = $trn->id;
                    $detail_transaksi->id_produk = $obat->obatalkes_id;
                    $detail_transaksi->jumlah = $request->jumlah[$i];
                    $detail_transaksi->discount = 0;
                    $detail_transaksi->subtotal = 0;
                    $save_detail = $detail_transaksi->save();
                    if ($save_detail){
                        $obat->stok = $obat->stok - $request->jumlah[$i];
                        $obat->save();    
                    }
                }
            }
            
                
                // StokM::where('id_produk', $request->id_produk)
                // ->update([
                //     'stok'      => (int)$stok_awal[0] - $request->jumlah
                // ]);
            return redirect('/transaksi')->with('status', 'Yatta!');
        }
        else
        {
            $transaksi      = DB::table('transaksi_umum_t')
                            ->where('created_at', $tanggal)
                            ->get();
                            
                            TransaksiUmum::create([
                                'kode_transaksi'    => rand(),
                                'total_harga'       => $total_harga,
                                'total_discount'    => 0,
                                'tunai'             => $request->tunai,
                                'kembali'           => 0,
                                'nama_pelanggan'    => $request->nama_pelanggan,
                                'alamat_pelanggan'  => $request->alamat_pelanggan,
                            ]);
                            // StokM::where('id_produk', $request->id_produk)
                            // ->update([
                            //     'stok'      => (int)$stok_awal[0] - $request->jumlah
                            // ]);
                        return redirect('/transaksi')->with('status', 'Yatta!');
        }
    }
}
