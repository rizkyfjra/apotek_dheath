<?php

namespace App\Http\Controllers;

use App\StokM;
use App\Kategori;
use App\JenisObat;
use App\Produk;
use App\Stok;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\TransaksiStok;

class StokMController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data   = DB::table('obatalkes_m')
                // ->join('produk_m', 'stok_m.id', '=', 'produk_m.id')
                // ->select('stok_m.*', 'produk_m.nama_produk')
                ->orderBy('obatalkes_m.obatalkes_nama')
                ->get();
            return datatables::of($data) //diambil dari StokM berdasarkan id
                ->addIndexColumn()
                ->editColumn('created_date', function ($data) {
                    return date('d M Y', strtotime($data->created_date)); //untuk menampilakn tanggal yang mudah dibaca user
                })
                ->editColumn('stok', function ($tabel) {
                    return number_format($tabel->stok) . ' Unit';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $kategori = Kategori::all();
        $produk = Produk::all();
        $jenis = JenisObat::all();
        $stok = Stok::all();
        $data = [
            'kategori' => $kategori,
            'jenis' => $jenis,
            'produk' => $produk,
            'stok'  => $stok
        ];
        return view('stok.index', $data);
    }

    public function store(Request $request)
    {
        $get_stok_data = DB::table('stok_m')
                        ->where('id_produk', '=', $request->id_produk)
                        ->count();
        if($get_stok_data > 0)
        {
            $stok_awal = DB::table('stok_m')
                        ->where('id_produk', '=', $request->id_produk)
                        ->select('*')
                        ->get();
            Stok::where('id', $stok_awal[0]->id)
                ->update([
                    'stok'  => ($stok_awal[0]->stok + $request->stok)
                ]);
            TransaksiStok::create([
                'id_stok'       => $stok_awal[0]->id,
                'stok_masuk'    => $request->stok,
                'stok_keluar'   => 0,
                'stok_retur'    => 0,
                'expired_at'    => $request->expired_at
            ]);
            return response()->json([
                'success'   => 'berhasil'
            ]);
        }
    }

    public function edit($id)
    {
        $stokm = StokM::find($id);
        return response()->json($stokm);
    }
    // public function detail(Request $request, $id)
    // {
    //     // $stokm = StokM::find($id);
    //     // return response()->json($stokm);

    //     if($request->ajax()) {
    //         $data   = DB::table('stok_t')
    //                 ->join('stok', 'stok.id', '=', 'stok_t.id_stok')
    //                 ->where('mst.stok.id_produk', '=', $id)
    //                 ->select('stok_t.*')
    //                 ->get();
    //         return datatables::of($data) //diambil dari StokM berdasarkan id
    //                 ->addIndexColumn()
    //                 ->editColumn('created_at', function ($data)
    //                 {
    //                     return date('d M Y', strtotime($data->created_at)); //untuk menampilakn tanggal yang mudah dibaca user
    //                 })
    //                 ->editColumn('stok', function ($tabel)
    //                 {

    //                 })
    //                 ->addColumn('action', function($row) {
    //                     $btn = '<a href="javascript:void(0)" data-id="'.$row->id.'"
    //                     class="btn btn-primary btn-sm editData">&nbsp;<i class="fa fa-info" aria-hidden="true"></i>&nbsp;</a>';
    //                     return $btn;
    //                 })
    //                 ->rawColumns(['action'])
    //                 ->make(true);
    //     }
    //     $kategori = Kategori::all();
    //     $produk = Produk::all();
    //     $jenis = JenisObat::all();
    //     $data = [
    //     'kategori' => $kategori,
    //     'jenis' => $jenis,
    //     'produk' => $produk
    //     ];
    //     return view('stok.index', $data);
    // }

    public function destroy($id)
    {
        StokM::destroy($id);
        return response()->json([
            'sucess'    => 'Data Berhasil Dihapus!'
        ]);
    }

    public function get_data(Request $request)
    {
        $stok   = DB::table('stok_m')
            ->where('stok', $request->stok)
            ->get();
        if ($request->id == null) {
            if (count($stok) > 0) {
                $id         = $stok[0]->id;
                $data       = DB::table('stok_m')
                    ->where('id', $id)
                    ->get();
                return response()->json($data);
            } else {
                $data = [];
                return response()->json($data);
            }
        } else {
            if (count($stok) > 0) {
                $id         = $request->id;
                $data       = DB::table('stok_m')
                    ->where('stok', $request->stok)
                    ->where('id', '<>', $id)
                    ->get();
                return response()->json($data);
            } else {
                $data = [];
                return response()->json($data);
            }
        }
    }
}
