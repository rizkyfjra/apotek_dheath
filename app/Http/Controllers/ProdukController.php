<?php

namespace App\Http\Controllers;

use App\Produk;
use App\Kategori;
use App\JenisObat;
use App\Stok;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class ProdukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax()) {
            $data   = DB::table('mst_produk')
                    ->join('mst_jenis_obat', 'mst_produk.id_jenis_obat', '=', 'mst_jenis_obat.id')
                    ->join('mst_kategori', 'mst_produk.id_kategori', '=', 'mst_kategori.id')
                    ->join('mst_stok', 'mst_produk.id', '=', 'mst_stok.id_produk')
                    ->select('mst_produk.*', 'mst_jenis_obat.jenis_obat', 'mst_kategori.kategori', 'mst_stok.stok')
                    ->orderBy('mst_produk.nama_produk')
                    ->get();
            return datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('image', function ($data)
                    {
                        if($data->foto_obat)
                        {
                            $url = url('img/upload/produk/'.$data->foto_obat);
                            return '<a href="'.$url.'" data-lightbox="'.$url.'" data-title="'.ucwords(strtolower($data->nama_produk)).'">
                            <img src="'.$url.'" width="100" heigth="100" class="img img-responsive img-thumbnail mx-auto d-block"></a>';
                        }
                        else
                        {
                            $url = url('img/upload/produk/obat.jpg');
                            return '<a href="'.$url.'" data-lightbox="'.$url.'" data-title="'.ucwords(strtolower($data->nama_produk)).'">
                            <img src="'.$url.'" width="100" heigth="100" class="img img-responsive img-thumbnail mx-auto d-block"></a>';
                        }
                    })
                    ->editColumn('harga', function ($data)
                    {
                        return 'Rp. '.number_format($data->harga);
                    })
                    ->editColumn('stok', function ($data)
                    {
                        return $data->stok.' Unit';
                    })
                    ->addColumn('action', function($row) {
                        $btn = '<a href="javascript:void(0)" data-id="'.$row->id.'" 
                        class="btn btn-primary btn-sm editData">Edit</a>
                        <a href="javascript:void(0)" data-id="'.$row->id.'" 
                        class="btn btn-danger btn-sm deleteData">Delete</a>';
                        return $btn;
                    })
                    ->rawColumns(['action', 'image'])
                    ->make(true);
        }
        $kategori   = Kategori::orderBy('kategori')->get();
        $jenis      = JenisObat::orderBy('jenis_obat')->get();
        $data       = [
            'kategori'  => $kategori,
            'jenis'     => $jenis
        ];
        return view('produk.index', $data);
    }

    public function store(Request $request)
    {
        $file                   = $request->file('foto_obat');
        if($file)
        {
            $file_name          = 'Produk'.str_replace(' ', '', $request->nama_produk).'.'.$file->getClientOriginalExtension();
            $destination        = public_path('img/upload/produk');
            $file->move($destination,$file_name);
        }
        else
        {
            $file_name          = null;
        }
        $id_produk = Produk::create([
            'nama_produk'       => $request->nama_produk,
            'id_jenis_obat'     => $request->id_jenis_obat,
            'id_kategori'       => $request->id_kategori,
            'harga'             => str_replace('.', '', $request->harga),
            'foto_obat'         => $file_name
        ])->id;
        Stok::create([
            'id_produk'         => $id_produk,
            'stok'              => 0
        ]);
        return redirect('/produk')->with('status', 'Data Produk Berhasil Ditambahkan!');
    }

    public function edit($id)
    {
        $produk = Produk::find($id);
        return response()->json($produk);
    }

    public function update(Request $request, $id)
    {
        $produk = Produk::find($id);
        if($request->file('foto_obat'))
        {
            $destination = public_path('img/upload/produk');
            if($produk->foto_obat != '' && $produk->foto_obat != null)
            {
                $foto_lama = $destination.'/'.$produk->foto_obat;
                unlink($foto_lama);
            }
            $file               = $request->file('foto_obat');
            $file_name          = 'Produk'.str_replace(' ', '', $request->nama_produk).'.'.$file->getClientOriginalExtension();
            $file->move($destination,$file_name);
            $produk->update([
                'nama_produk'       => $request->nama_produk,
                'id_jenis_obat'     => $request->id_jenis_obat,
                'id_kategori'       => $request->id_kategori,
                'harga'             => str_replace('.', '', $request->harga),
                'foto_obat'         => $file_name
            ]);
            return redirect('/produk')->with('status', 'Data Produk Berhasil Disunting!');
        }
        else
        {
            $produk->update([
                'nama_produk'       => $request->nama_produk,
                'id_jenis_obat'     => $request->id_jenis_obat,
                'id_kategori'       => $request->id_kategori,
                'harga'             => str_replace('.', '', $request->harga)
            ]);
            return redirect('/produk')->with('status', 'Data Produk Berhasil Disunting!');
        }
    }

    public function destroy($id)
    {
        $produk = Produk::find($id);
        if($produk->foto_obat != '' && $produk->foto_obat != null)
        {
            $destination    = public_path('img/upload/produk');
            $foto           = $destination.'/'.$produk->foto_obat;
            unlink($foto);
        }
        DB::table('mst_stok')
        ->where('id_produk', '=', $id)
        ->delete();
        Produk::destroy($id);
        return response()->json([
            'sucess'    => 'Data Berhasil Dihapus!'
        ]);
    }
}
