<?php

namespace App\Http\Controllers;

use App\Pasien;
use App\Klinik;
use App\Vendor;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ResepMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if($request->ajax()){
            $data = Pasien::all()->sortBy('pasien_id');
            return datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $btn = '<a href="javascript:void(0)" data-pasien_id="'.$row->pasien_id.'"
                    class="btn btn-primary btn-sm editData">Edit</a>
                    <a href="javascript:void(0)" data-pasien_id="'.$row->pasien_id.'"
                    class="btn btn-danger btn-sm deleteData">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);

        }
        return view('resep.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Dokter::updateOrCreate([
        'id_pegawai' => $request->id_pegawai
        ],
        [
        'nama_pegawai' => $request->nama_pegawai,
        'tempat_lahir' => $request->tempat_lahir,
        'tanggal_lahir' => $request->tanggal_lahir,
        'jenis_kelamin' => $request->jenis_kelamin,
        'agama' => $request->agama,
        ]
        );
        return response()->json([
            'success'   => 'Transaksi Data Berhasil!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_pegawai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($pasien_id)
    {
        $pasien = Pasien::find($pasien_id);
        return response()->json($pasien);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($pasien_id)
    {
        $pasien = Pasien::find($pasien_id);
        return response()->json($pasien);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_pegawai)
    {
        Dokter::destroy($id_pegawai);
        return response()->json([
            'sucess'    => 'Data Vendor Berhasil Dihapus!'
        ]);
    }
}
