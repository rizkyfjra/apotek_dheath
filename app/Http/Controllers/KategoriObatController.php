<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class KategoriObatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Kategori::all()->sortBy('kategori');
            return datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row) {
                        $btn = '<a href="javascript:void(0)" data-id="'.$row->id.'" 
                        class="btn btn-primary btn-sm editData">Edit</a>
                        <a href="javascript:void(0)" data-id="'.$row->id.'" 
                        class="btn btn-danger btn-sm deleteData">Delete</a>';
                        return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('kategori.index');
    }

    public function store(Request $request)
    {
        Kategori::updateOrCreate([
                'id'        => $request->id
            ],
            [
                'kategori'  => $request->kategori
            ]
        );
        return response()->json([
            'success'   => 'Transaksi Data Berhasil!'
        ]);
    }

    public function edit($id)
    {
        $kategori = Kategori::find($id);
        return response()->json($kategori);
    }

    public function destroy($id)
    {
        Kategori::destroy($id);
        return response()->json([
            'sucess'    => 'Data Berhasil Dihapus!'
        ]);
    }

    public function get_data(Request $request)
    {
        $kategori   = DB::table('kategori_m')
                    ->where('kategori', $request->kategori)
                    ->get();
        if($request->id == null)
        {
            if(count($kategori) > 0)
            {
                $id         = $kategori[0]->id;
                $data       = DB::table('kategori_m')
                            ->where('id', $id)
                            ->get();
                return response()->json($data);
            }
            else
            {
                $data = [];
                return response()->json($data);
            }
        }
        else
        {
            if(count($kategori) > 0)
            {
                $id         = $request->id;
                $data       = DB::table('kategori_m')
                            ->where('kategori', $request->kategori)
                            ->where('id', '<>', $id)
                            ->get();
                return response()->json($data);
            }
            else
            {
                $data = [];
                return response()->json($data);
            }
        }
    }
}