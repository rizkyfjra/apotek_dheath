<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Klinik extends Model
{
    protected $table = 'klinik_m';
    protected $fillable = ['klinik', 'alamat'];
}
