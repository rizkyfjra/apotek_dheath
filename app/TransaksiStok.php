<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiStok extends Model
{
    protected $table = 'stok_t';
    protected $fillable = [
        'id_stok', 'stok_masuk', 'stok_keluar',
        'stok_retur', 'expired_at'
    ];
}
