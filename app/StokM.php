<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StokM extends Model
{
    protected $table = 'stok_m';
    protected $fillable = ['id', 'id_produk', 'stok', 'expired_at', 'created_at', 'update_at'];
}
