<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    protected $table = 'stok_m';
    protected $fillable = ['id_produk', 'stok'];
}
